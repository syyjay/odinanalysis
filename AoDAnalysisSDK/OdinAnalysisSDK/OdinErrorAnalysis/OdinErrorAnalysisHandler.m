//
//  OdinErrorAnalysisHandler.m
//  OdinAnalysisSDK
//
//  Created by nathan on 2019/3/1.
//  Copyright © 2019 Odin. All rights reserved.
//

#import "OdinErrorAnalysisHandler.h"
#include <libkern/OSAtomic.h>
#include <execinfo.h>

static NSString * const Odin_UncaughtExceptionHandlerSignalExceptionName = @"UncaughtExceptionHandlerSignalExceptionName";
static NSString * const Odin_UncaughtExceptionHandlerSignalKey = @"UncaughtExceptionHandlerSignalKey";

static volatile int32_t UncaughtExceptionCount = 0;
static const int32_t UncaughtExceptionMaximum = 20;


@interface OdinErrorAnalysisHandler()
@property (nonatomic) NSUncaughtExceptionHandler *defaultExceptionHandler;
@property (nonatomic, unsafe_unretained) struct sigaction *prev_signal_handlers;
@end

@implementation OdinErrorAnalysisHandler
+ (instancetype)sharedHandler {
    static OdinErrorAnalysisHandler *sharedHandler = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedHandler = [[OdinErrorAnalysisHandler alloc] init];
    });
    return sharedHandler;
}

+ (NSArray *)backtrace;
{
    void* callstack[128];
    int frames = backtrace(callstack, 128);
    char **strs = backtrace_symbols(callstack,frames);
    
    NSMutableArray *backtrace = [NSMutableArray arrayWithCapacity:frames];
    for (int i=0;i<frames;i++) {
        [backtrace addObject:[NSString stringWithUTF8String:strs[i]]];
    }
    free(strs);
    return backtrace;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        // Create a hash table of weak pointers to SensorsAnalytics instances
        
        _prev_signal_handlers = calloc(NSIG, sizeof(struct sigaction));
        
        // Install our handler
        [self setupHandlers];
    }
    return self;
}

- (void)setupHandlers {
    _defaultExceptionHandler = NSGetUncaughtExceptionHandler();
    NSSetUncaughtExceptionHandler(&OdinHandleException);
    
    struct sigaction action;
    sigemptyset(&action.sa_mask);
    action.sa_flags = SA_SIGINFO;
    action.sa_sigaction = &OdinSignalHandler;
    int signals[] = {SIGABRT, SIGILL, SIGSEGV, SIGFPE, SIGBUS, SIGPIPE};
    for (int i = 0; i < sizeof(signals) / sizeof(int); i++) {
        struct sigaction prev_action;
        int err = sigaction(signals[i], &action, &prev_action);
        if (err == 0) {
            memcpy(_prev_signal_handlers + signals[i], &prev_action, sizeof(prev_action));
        } else {
//            Odin_Error(@"Errored while trying to set up sigaction for signal %d", signals[i]);
        }
    }
}

static void OdinSignalHandler(int crashSignal, struct __siginfo *info, void *context) {
    OdinErrorAnalysisHandler *handler = [OdinErrorAnalysisHandler sharedHandler];
    
    int32_t exceptionCount = OSAtomicIncrement32(&UncaughtExceptionCount);
    if (exceptionCount <= UncaughtExceptionMaximum) {
        NSDictionary *userInfo = @{Odin_UncaughtExceptionHandlerSignalKey: @(crashSignal)};
        NSString *reason;
        @try {
            reason = [NSString stringWithFormat:@"Signal %d was raised.", crashSignal];
        } @catch(NSException *exception) {
            //ignored
        }
        
        @try {
            NSException *exception = [NSException exceptionWithName:Odin_UncaughtExceptionHandlerSignalExceptionName
                                                             reason:reason
                                                           userInfo:userInfo];
            
            [handler handleUncaughtException:exception];
        } @catch(NSException *exception) {
            
        }
    }
    
    struct sigaction prev_action = handler.prev_signal_handlers[crashSignal];
    if (prev_action.sa_flags & SA_SIGINFO) {
        if (prev_action.sa_sigaction) {
            prev_action.sa_sigaction(crashSignal, info, context);
        }
    } else if (prev_action.sa_handler) {
        prev_action.sa_handler(crashSignal);
    }
}

static void OdinHandleException(NSException *exception) {
    OdinErrorAnalysisHandler *handler = [OdinErrorAnalysisHandler sharedHandler];
    
    int32_t exceptionCount = OSAtomicIncrement32(&UncaughtExceptionCount);
    if (exceptionCount <= UncaughtExceptionMaximum) {
        [handler handleUncaughtException:exception];
    }
    
    if (handler.defaultExceptionHandler) {
        handler.defaultExceptionHandler(exception);
    }
}


- (void)handleUncaughtException:(NSException *)exception {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    
    //记录一次session结束事件、页面停留事件
    [OdinDataManager shareInstance].headDic[@"network_type"]=[[OdinConfigManager shareInstance] getWorkType];
    
    NSDictionary *sessionEndDic=@{@"eventType":[NSNumber numberWithUnsignedInteger:OdinEventTypeSession],
                                  @"attributs":@{@"sessionType":@"2",
                                                 @"du":[NSNumber numberWithInteger:ABS([OdinTimeTool getDiffTime:[OdinSessionManager shareInstance].sessionStartTime endTime:[OdinTimeTool getCheckCurrentTime]]*1000)],
                                                 @"session_id":[OdinSessionManager shareInstance].session.session_id,
                                                 @"stat_time":[OdinTimeTool getCheckCurrentTime],
                                                 @"headInfo":[NSString aod_jsonString:[OdinDataManager shareInstance].headDic],
                                                 @"duStartTime":[OdinSessionManager shareInstance].sessionStartTime
                                                 }
                                  
                                  };
    
    UIViewController *currentVc=[[OdinErrorAnalysisHandler sharedHandler] getCurrentVC];
    NSArray *vcs=[currentVc.navigationController viewControllers];
    NSInteger index=[vcs indexOfObject:currentVc];
    UIViewController *lastVc;
    if (index>0) {
        lastVc=vcs[index-1];
    }
    NSDictionary *pageEndDic=@{@"eventType":[NSNumber numberWithUnsignedInteger:OdinEventTypePageView],
                               @"attributs":@{@"page_id":NSStringFromClass([currentVc class]),
                                              @"prepage_id":(lastVc? NSStringFromClass([lastVc class]):@"UNKNOWN"),
                                              @"du":[NSNumber numberWithInteger:ABS([OdinTimeTool getDiffTime:[OdinDataManager shareInstance].viewAppearTimeStr endTime:[OdinTimeTool getCheckCurrentTime]]*1000)],
                                              @"session_id":[OdinSessionManager shareInstance].session.session_id,
                                              @"stat_time":[OdinTimeTool getCheckCurrentTime],@"headInfo":[NSString aod_jsonString:[OdinDataManager shareInstance].headDic],
                                               @"duStartTime":[OdinDataManager shareInstance].viewAppearTimeStr
                                              }
                               
                               };
     NSMutableArray *eventArr=[NSMutableArray arrayWithArray:@[sessionEndDic,pageEndDic]];
    [eventArr writeToFile:[NSString aod_getCacheEventDataPath] atomically:YES];
    

    @try {
        // 异常的堆栈信息
        //  NSArray *stackArray = [exception callStackSymbols];
        // 出现异常的原因
        NSString *reason = [exception reason];
        // 异常名称
        NSString *name = [exception name];
        
        [dic setObject:name forKey:@"title"];
        [dic setObject:reason forKey:@"context"];
        
    } @catch (NSException *exception) {
        
//        Odin_Error(@"exception==%@",exception);
    }
    
    if ([dic allKeys].count==0) {
        [dic setObject:@"未知原因" forKey:@"title"];
    }
    dic[@"category"]=@"1";
    //存储崩溃信息 崩溃不用异步执行
    [[OdinDataManager shareInstance]  saveEventData:OdinEventTypeError attributs:dic ohterAttributs:nil complete:nil];
}



- (UIViewController *)getCurrentVC{
    UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *currentVC = [self getCurrentVCFrom:rootViewController];
    return currentVC;
}



- (UIViewController *)getCurrentVCFrom:(UIViewController *)rootVC{
    UIViewController *currentVC;
    if ([rootVC presentedViewController]) {
        // 视图是被presented出来的
        rootVC = [rootVC presentedViewController];
    }
    if ([rootVC isKindOfClass:[UITabBarController class]]) {
        // 根视图为UITabBarController
        currentVC = [self getCurrentVCFrom:[(UITabBarController *)rootVC selectedViewController]];
    } else if ([rootVC isKindOfClass:[UINavigationController class]]){
        // 根视图为UINavigationController
        
        currentVC = [self getCurrentVCFrom:[(UINavigationController *)rootVC visibleViewController]];
    } else {
        // 根视图为非导航类
        currentVC = rootVC;
    }
    return currentVC;
}



@end
