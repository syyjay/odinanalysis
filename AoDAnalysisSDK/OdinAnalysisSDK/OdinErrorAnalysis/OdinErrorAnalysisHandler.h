//
//  OdinErrorAnalysisHandler.h
//  OdinAnalysisSDK
//
//  Created by nathan on 2019/3/1.
//  Copyright © 2019 Odin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OdinErrorAnalysisHandler : NSObject
+ (instancetype)sharedHandler;
- (UIViewController *)getCurrentVC;
@end

NS_ASSUME_NONNULL_END
