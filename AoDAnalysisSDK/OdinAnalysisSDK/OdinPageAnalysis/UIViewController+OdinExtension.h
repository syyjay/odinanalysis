//
//  UIViewController+AoDExtension.h
//  AoDAnalysisSDK
//
//  Created by nathan on 2019/3/1.
//  Copyright © 2019 AoD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (OdinExtension)
+ (void)odin_swizzIt;
@end

NS_ASSUME_NONNULL_END
