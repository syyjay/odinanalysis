//
//  UIViewController+OdinExtension.m
//  OdinAnalysisSDK
//
//  Created by nathan on 2019/3/1.
//  Copyright © 2019 Odin. All rights reserved.
//

#import "UIViewController+OdinExtension.h"
#import <objc/runtime.h>

@implementation UIViewController (OdinExtension)

+(void)odin_swizzIt
{
    swizzInstance([self class],@selector(viewDidAppear:),@selector(swizzviewDidAppear:));
    swizzInstance([self class],@selector(viewDidDisappear:),@selector(swizzviewDidDisAppear:));
}

static void swizzInstance(Class class, SEL originalSelector, SEL swizzledSelector)
{
    Method originalMethod = class_getInstanceMethod(class, originalSelector);
    Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
    
    BOOL didAddMethod = class_addMethod(class, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod));
    
    if (didAddMethod)
    {
        class_replaceMethod(class, swizzledSelector, method_getImplementation(originalMethod),method_getTypeEncoding(originalMethod));
    }
    else
    {
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }
}

#pragma mark 视图出现消失方法
-(void)swizzviewDidAppear:(BOOL)animated
{
    NSString *pageId;
    UIViewController *paVc =  [self parentViewController];
    if ([self isKindOfClass:[UITabBarController class]]||[self isKindOfClass:[UINavigationController class]]||[self isKindOfClass:[UISplitViewController class]]||[self isKindOfClass:[UISplitViewController class]]) {
        //不做处理
    }else{
        if ([self isKindOfClass:[UIPageViewController class]]) {
            //特殊处理
        }else if ([paVc isKindOfClass:[UIPageViewController class]]){

        }
        else{
             pageId = NSStringFromClass(self.class);
        }
    }
    
    if (pageId) {
        [OdinDataManager shareInstance].viewAppearTimeStr=[OdinTimeTool getCheckCurrentTime];
    }
  
    // Call the original method (viewWillAppear)
    [self swizzviewDidAppear:animated];
}

- (void)swizzviewDidDisAppear:(BOOL)animated
{
    NSString *pageId;
    UIViewController *paVc =  [self parentViewController];
    if ([self isKindOfClass:[UITabBarController class]]||[self isKindOfClass:[UINavigationController class]]||[self isKindOfClass:[UISplitViewController class]]||[self isKindOfClass:[UISplitViewController class]]||[self isKindOfClass: NSClassFromString(@"UIInputWindowController")]||[self isKindOfClass:NSClassFromString(@"UICompatibilityInputViewController")]) {
        //不做处理
    }else{
        if ([self isKindOfClass:[UIPageViewController class]]) {
            //特殊处理
//            pageId = NSStringFromClass(self.class);不处理
            [[OdinDataManager shareInstance].pageManagerArr removeAllObjects];
        }else if ([paVc isKindOfClass:[UIPageViewController class]]){
//            pageId=[NSString stringWithFormat:@"UIPageViewController/%@",NSStringFromClass(self.class)];
            //不处理
            
        }else{
            pageId = NSStringFromClass(self.class);
        }
    }
    //存本地
    if (pageId) {
        NSString *prePageId=@"UNKNOWN";
        if ([OdinDataManager shareInstance].lastVc) {
            prePageId=NSStringFromClass([[OdinDataManager shareInstance].lastVc class]);
        }
        [[OdinDataManager shareInstance] saveEventData:OdinEventTypePageView attributs:@{@"page_id":pageId,@"prepage_id":prePageId,@"du":[NSNumber numberWithInteger:[OdinTimeTool getDiffTime:[OdinDataManager shareInstance].viewAppearTimeStr endTime:[OdinTimeTool getCheckCurrentTime]]*1000]} ohterAttributs:@{@"duStartTime":[OdinDataManager shareInstance].viewAppearTimeStr} complete:nil];
         [OdinDataManager shareInstance].lastVc=self;
    }
   
    [self swizzviewDidDisAppear:animated];
}

@end
