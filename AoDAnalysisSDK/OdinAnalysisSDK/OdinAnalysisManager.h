//
//  OdinAnalysisManager.h
//  OdinAnalysisSDK
//
//  Created by nathan on 2019/2/27.
//  Copyright © 2019 Odin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OdinConfig/OdinConfigManager.h"
#import "OdinUtils/OdinSingleton.h"

NS_ASSUME_NONNULL_BEGIN

@interface OdinAnalysisManager : NSObject

aod_singleton_interface(OdinAnalysisManager)

/**
 SDK是否 没有init完成 默认NO表示已初始化完成 YES表示没有初始化完成
 */
@property(nonatomic,assign) BOOL sdkInitComplete;
@property(nonatomic,assign) OdinAppStatus appStatus;
/**
 校对事件的 时间差(单位秒)
 */
@property(nonatomic,assign) NSTimeInterval diffTime;
/**
 拉取配置和发送策略
 */
- (void)loadConfig:(void (^)(void))compelte;

/**
 校对时间
 */
- (void)checkTime:(nullable void (^)(void))compete;


/**
 检测是否是首次启动
 */
- (void)firstLauch;

/**
 开始收集数据
 */
- (void)startAnalysis:(OdinAppStatus)appStatus;

/**
 停止收集数据
 */
- (void)stopAnalysis;

/**
 开始
 */
- (void)startMonitoring:(nullable void (^)(void))compelte;

/**
 启动时发送一次数据
 */
- (void)sendDataByLauch;
@end

NS_ASSUME_NONNULL_END
