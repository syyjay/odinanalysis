//
//  OdinGzipHelper.m
//  OdinAnalyticsSDK
//
//  Created by nathan on 2019/1/24.
//  Copyright © 2019 Odin. All rights reserved.
//

#import "OdinGzipHelper.h"
#import "zlib.h"
//#import "OdinLogHelper.h"

@implementation OdinGzipHelper

+(NSData*) gzipData: (NSData*)pUncompressedData
{

    if (!pUncompressedData || [pUncompressedData length] == 0)
    {
//        Odin_Error(@" Error: Can't compress an empty or null NSData object.");
        return nil;
    }
    
    int deflateStatus;
    float buffer = 1.1;
    do {
        
        z_stream zlibStreamStruct;
        zlibStreamStruct.zalloc    = Z_NULL; // Set zalloc, zfree, and opaque to Z_NULL so
        zlibStreamStruct.zfree     = Z_NULL; // that when we call deflateInit2 they will be
        zlibStreamStruct.opaque    = Z_NULL; // updated to use default allocation functions.
        zlibStreamStruct.total_out = 0; // Total number of output bytes produced so far
        zlibStreamStruct.next_in   = (Bytef*)[pUncompressedData bytes]; // Pointer to input bytes
        zlibStreamStruct.avail_in  = (uInt)[pUncompressedData length]; // Number of input bytes left to process
        
        int initError = deflateInit2(&zlibStreamStruct, Z_DEFAULT_COMPRESSION, Z_DEFLATED, (15+16), 8, Z_DEFAULT_STRATEGY);
        if (initError != Z_OK)
        {
            NSString *errorMsg = nil;
            switch (initError)
            {
                case Z_STREAM_ERROR:
                    errorMsg = @"Invalid parameter passed in to function.";
                    break;
                case Z_MEM_ERROR:
                    errorMsg = @"Insufficient memory.";
                    break;
                case Z_VERSION_ERROR:
                    errorMsg = @"The version of zlib.h and the version of the library linked do not match.";
                    break;
                default:
                    errorMsg = @"Unknown error code.";
                    break;
            }
//            Odin_Error(@"deflateInit2() Error: \"%@\" Message: \"%s\"", errorMsg, zlibStreamStruct.msg);
            return nil;
        }
        
        // Create output memory buffer for compressed data. The zlib documentation states that
        // destination buffer size must be at least 0.1% larger than avail_in plus 12 bytes.
        NSMutableData *compressedData = [NSMutableData dataWithLength:[pUncompressedData length] * buffer + 12];
        
        do {
            // Store location where next byte should be put in next_out
            zlibStreamStruct.next_out = [compressedData mutableBytes] + zlibStreamStruct.total_out;
            
            // Calculate the amount of remaining free space in the output buffer
            // by subtracting the number of bytes that have been written so far
            // from the buffer's total capacity
            zlibStreamStruct.avail_out = (uInt)([compressedData length] - zlibStreamStruct.total_out);
            
            /* deflate() compresses as much data as possible, and stops/returns when
             the input buffer becomes empty or the output buffer becomes full. If
             deflate() returns Z_OK, it means that there are more bytes left to
             compress in the input buffer but the output buffer is full; the output
             buffer should be expanded and deflate should be called again (i.e., the
             loop should continue to rune). If deflate() returns Z_STREAM_END, the
             end of the input stream was reached (i.e.g, all of the data has been
             compressed) and the loop should stop. */
            deflateStatus = deflate(&zlibStreamStruct, Z_FINISH);
            
        } while ( deflateStatus == Z_OK );
        
        if (deflateStatus == Z_BUF_ERROR && buffer < 32) {
            continue;
        }
        
        // Check for zlib error and convert code to usable error message if appropriate
        if (deflateStatus != Z_STREAM_END) {
            NSString *errorMsg = nil;
            switch (deflateStatus)
            {
                case Z_ERRNO:
                    errorMsg = @"Error occured while reading file.";
                    break;
                case Z_STREAM_ERROR:
                    errorMsg = @"The stream state was inconsistent (e.g., next_in or next_out was NULL).";
                    break;
                case Z_DATA_ERROR:
                    errorMsg = @"The deflate data was invalid or incomplete.";
                    break;
                case Z_MEM_ERROR:
                    errorMsg = @"Memory could not be allocated for processing.";
                    break;
                case Z_BUF_ERROR:
                    errorMsg = @"Ran out of output buffer for writing compressed bytes.";
                    break;
                case Z_VERSION_ERROR:
                    errorMsg = @"The version of zlib.h and the version of the library linked do not match.";
                    break;
                default:
                    errorMsg = @"Unknown error code.";
                    break;
            }
//            Odin_Error(@"zlib error while attempting compression: \"%@\" Message: \"%s\"", errorMsg, zlibStreamStruct.msg);
            
            // Free data structures that were dynamically created for the stream.
            deflateEnd(&zlibStreamStruct);
            
            return nil;
        }
        
        // Free data structures that were dynamically created for the stream.
        deflateEnd(&zlibStreamStruct);
        [compressedData setLength: zlibStreamStruct.total_out];
        
        return compressedData;
    } while ( false );
    return nil;
}


@end
