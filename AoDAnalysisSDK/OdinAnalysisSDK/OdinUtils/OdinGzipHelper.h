//
//  OdinGzipHelper.h
//  OdinAnalyticsSDK
//
//  Created by nathan on 2019/1/24.
//  Copyright © 2019 Odin. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface OdinGzipHelper : NSObject

/**
 *  压缩数据
 */
+(NSData*)gzipData:(NSData*)pUncompressedData;

@end

