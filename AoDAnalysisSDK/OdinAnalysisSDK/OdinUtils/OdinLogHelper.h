//
//  OdinLogHelper.h
//  OdinAnalyticsSDK
//
//  Created by nathan on 2019/1/14.
//  Copyright © 2019 Odin. All rights reserved.
//

#import <Foundation/Foundation.h>



typedef NS_ENUM(NSInteger, localLogType) {
    localLogType_datasend    = 0,
    localLogType_nettime,
    localLogType_config,
    localLogType_register,
    localLogType_undefine,
};


#define OdinLog_Level     [OdinLogHelper sharedInstance].logLevel

typedef NS_ENUM(NSInteger, OdinLogLevel) {
    OdinLogLevel_None    = 0,
    OdinLogLevel_Info    = 20,
    OdinLogLevel_Debug   = 20,
    OdinLogLevel_Warn    = 60,
    OdinLogLevel_Error   = 80,
    OdinLogLevel_All     = 100
};


//#define Odin_Info(fmt,...)                          \
//if(OdinLogLevel_Info<=OdinLog_Level){            \
//    NSLog((@"%s-Line%d-Info:" fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);    \
//}
//
//#define Odin_Debug(fmt,...)                          \
//if(OdinLogLevel_Debug<=OdinLog_Level){            \
//    NSLog((@"%s-Line%d-Debug:" fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);    \
//}
//
//#define Odin_Warn(fmt,...)                          \
//if(OdinLogLevel_Warn<=OdinLog_Level){            \
//    NSLog((@"%s-Line%d-Warn:" fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);    \
//}
//
//#define Odin_Error(fmt,...)                          \
//if(OdinLogLevel_Error<=OdinLog_Level){            \
//    NSLog((@"%s-Line%d-Error:" fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);    \
//}

#define Odin_Info(fmt,...)                          \
if(OdinLogLevel_Info<=OdinLog_Level){            \
NSLog((@"-Info:" fmt), ##__VA_ARGS__);    \
}

#define Odin_Debug(fmt,...)                          \
if(OdinLogLevel_Debug<=OdinLog_Level){            \
NSLog((@"-Debug:" fmt), ##__VA_ARGS__);    \
}

#define Odin_Warn(fmt,...)                          \
if(OdinLogLevel_Warn<=OdinLog_Level){            \
NSLog((@"-Warn:" fmt), ##__VA_ARGS__);    \
}

#define Odin_Error(fmt,...)                          \
if(OdinLogLevel_Error<=OdinLog_Level){            \
NSLog((@"-Error:" fmt), ##__VA_ARGS__);    \
}


NS_ASSUME_NONNULL_BEGIN

@interface OdinLogHelper : NSObject

@property(nonatomic,assign)int logLevel;

+ (instancetype)sharedInstance;

/**
 *  设置打印输出级别
 *  @param  level  级别
 */
- (void)setLevel:(int)level;

/**
 *  测试使用，输入本地文件
 *  @param  dataType  类型
 */
- (void)writeDataWithType:(localLogType)dataType text:(NSString *)dataText;



@end

NS_ASSUME_NONNULL_END
