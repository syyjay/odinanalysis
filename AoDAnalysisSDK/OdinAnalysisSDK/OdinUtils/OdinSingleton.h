//
//  AodSingleton.h
//  AoDAnalysisSDK
//
//  Created by nathan on 2019/3/7.
//  Copyright © 2019 AoD. All rights reserved.
//

#ifndef AodSingleton_h
#define AodSingleton_h

// .h
#define aod_singleton_interface(class)  + (instancetype)shareInstance;

// .m
#define aod_singleton_implementation(class) \
static class *_instance; \
\
+ (id)allocWithZone:(struct _NSZone *)zone \
{ \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [super allocWithZone:zone]; \
}); \
\
return _instance; \
} \
\
+ (instancetype)shareInstance \
{ \
if (_instance == nil) { \
_instance = [[class alloc] init]; \
} \
\
return _instance; \
}
#endif /* AodSingleton_h */
