//
//  OdinTimeTool.h
//  OdinAnalysisSDK
//
//  Created by nathan on 2019/2/27.
//  Copyright © 2019 Odin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OdinTimeTool : NSObject

/**
 本地的当前时间

 @return 时间字符串
 */
+ (NSString *)getLocalCurrentTime;

/**
 校验过后的时间

 @return 时间字符串
 */
+ (NSString *)getCheckCurrentTime;

+ (NSString *)getCheckCurrentTime:(NSString *)fromatter;
/**
 计算两个时间的时间差

 @param startTime 开始时间
 @param endTime   结束时间
 @return 时间差 单位秒
 */
+ (NSTimeInterval)getDiffTime:(NSString *)startTime endTime:(NSString *)endTime;


/**
 将时间转为时间戳
 */
+ (NSTimeInterval)timeSwitchTimestamp:(NSString *)timeStr;

+ (NSString*)timestampSwitchTimestring:(NSDate *)date;
@end

NS_ASSUME_NONNULL_END
