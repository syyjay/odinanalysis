//
//  OdinTimeTool.m
//  OdinAnalysisSDK
//
//  Created by nathan on 2019/2/27.
//  Copyright © 2019 Odin. All rights reserved.
//

#import "OdinTimeTool.h"

@implementation OdinTimeTool
+ (NSString *)getLocalCurrentTime{
    NSDate *currentDate = [NSDate date];//获取当前时间，日期
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];// 创建一个时间格式化对象
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];//设定时间格式,这里可以设置成自己需要的格式
    NSString *dateString = [dateFormatter stringFromDate:currentDate];//将时间转化成字符串
    return dateString;
}

+ (NSString *)getCheckCurrentTime{
    NSDate *checkCurrentDate=[NSDate dateWithTimeIntervalSinceNow:[OdinAnalysisManager shareInstance].diffTime];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];// 创建一个时间格式化对象
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];//设定时间格式,这里可以设置成自己需要的格式
    NSString *dateString = [dateFormatter stringFromDate:checkCurrentDate];//将时间转化成字符串
    return dateString;
}

+ (NSString *)getCheckCurrentTime:(NSString *)fromatter{
    NSDate *checkCurrentDate=[NSDate dateWithTimeIntervalSinceNow:[OdinAnalysisManager shareInstance].diffTime];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];// 创建一个时间格式化对象
    [dateFormatter setDateFormat:fromatter];//设定时间格式,这里可以设置成自己需要的格式
    NSString *dateString = [dateFormatter stringFromDate:checkCurrentDate];//将时间转化成字符串
    return dateString;
}

+ (NSTimeInterval)getDiffTime:(NSString *)startTime endTime:(NSString *)endTime{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];// 创建一个时间格式化对象
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];//设定时间格式,这里可以设置成自己需要的格式
    NSDate *startDate=[dateFormatter dateFromString:startTime];
    NSDate *endDate=[dateFormatter dateFromString:endTime];
    NSTimeInterval diffTime= ABS([endDate timeIntervalSinceDate:startDate]);
    return diffTime;
}

+ (NSTimeInterval)timeSwitchTimestamp:(NSString *)timeStr{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];// 创建一个时间格式化对象
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];//设定时间格式,这里可以设置成自己需要的格式
    NSDate *startDate=[dateFormatter dateFromString:timeStr];
    return [startDate timeIntervalSince1970];
}

+ (NSString*)timestampSwitchTimestring:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];// 创建一个时间格式化对象
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];//设定时间格式,这里可以设置成自己需要的格式
    NSString *dateString = [dateFormatter stringFromDate:date];//将时间转化成字符串
    return dateString;
}
@end
