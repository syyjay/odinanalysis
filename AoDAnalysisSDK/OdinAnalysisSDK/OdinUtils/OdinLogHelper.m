//
//  OdinLogHelper.m
//  OdinAnalyticsSDK
//
//  Created by nathan on 2019/1/14.
//  Copyright © 2019 Odin. All rights reserved.
//

#import "OdinLogHelper.h"
@implementation OdinLogHelper
{
    BOOL initlog;
    
    NSString *dataSendPath; //发送数据log
    
    NSString *netTimePath; //网络校时log
    
    NSString *configPath; //请求配置log
    
    NSString *registerPath; //注册渠道log
 
}

+ (instancetype)sharedInstance
{
    static OdinLogHelper *helper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        helper = [[self alloc] init];
    });
    
    return helper;
}

- (instancetype)init
{
    if (self = [super init]) {
        
        if ([OdinSDKTYPEMDE isEqualToString:@"SupportTest"]) {
            //正式上传需关闭 禁用该功能
            [self initLog];
            
        }else{
            
        }
    }
    return self;
}

- (void)setLevel:(int)level;
{
    self.logLevel = level;
}

- (void)initLog
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentDirectory = [paths objectAtIndex:0];
    
    //
    dataSendPath = [documentDirectory stringByAppendingPathComponent:@"OdinDataSend"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:dataSendPath withIntermediateDirectories:YES attributes:nil error:nil];
    
    //
    netTimePath = [documentDirectory stringByAppendingPathComponent:@"OdinNetTime"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:netTimePath withIntermediateDirectories:YES attributes:nil error:nil];
    
    //
    configPath = [documentDirectory stringByAppendingPathComponent:@"OdinConfig"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:configPath withIntermediateDirectories:YES attributes:nil error:nil];
    
    //
    registerPath = [documentDirectory stringByAppendingPathComponent:@"OdinRegister"];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:registerPath withIntermediateDirectories:YES attributes:nil error:nil];
    
    
    initlog = YES;
}

- (void)writeDataWithType:(localLogType)dataType text:(NSString *)dataText
{
    
    if (!initlog) {
        return;
    }
    NSString *currentTimeStr = [OdinTimeTool getCheckCurrentTime];
    NSString *currentPath;
    switch (dataType) {
        case localLogType_datasend:
        {
            currentPath = dataSendPath;
        }
            break;
        case localLogType_nettime:
        {
            currentPath = netTimePath;
        }
            break;
        case localLogType_config:
        {
            currentPath = configPath;
        }
            break;
        case localLogType_register:
        {
            currentPath = registerPath;
        }
            break;
        default:
            break;
    }
    
   NSString *path = [NSString stringWithFormat:@"%@/%@.log",currentPath,currentTimeStr];
    NSError *error;
    [dataText writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:&error];

    if (error) {
        Odin_Debug(@"写入失败:%@",error);
    }else{
        Odin_Debug(@"写入成功");
    }
}





@end
