//
//  OdinConfigManager.h
//  OdinAnalysisSDK
//
//  Created by nathan on 2019/2/28.
//  Copyright © 2019 Odin. All rights reserved.
//

typedef enum : NSUInteger {
    OdinAppStatusUsing,
    OdinAppStatusStart,
    OdinAppStatusBackground,
} OdinAppStatus;

#import <Foundation/Foundation.h>
@class OdinConfig;
NS_ASSUME_NONNULL_BEGIN

@interface OdinConfigManager : NSObject
+(instancetype)shareInstance;
//默认策略
@property(nonatomic,strong) OdinConfig *defaultConfig;
//当前使用的策略
@property(nonatomic,strong) OdinConfig *useConfig;

/**
 发送的数量

 @return 发送的数据条数
 */
- (NSInteger)getSendCount:(OdinAppStatus)appStatus;

- (NSInteger)getTimeInterval:(OdinAppStatus)appStatus;

/**
 获取网络类型

 @return 网络类型字符串
 */
-(NSString *)getWorkType;

/**
 开始监听网络
 */
- (void)startMonitoring:(void (^)(void))compelte;
- (void)setComplteBlock:(nullable void (^)(void))block;
@end

NS_ASSUME_NONNULL_END
