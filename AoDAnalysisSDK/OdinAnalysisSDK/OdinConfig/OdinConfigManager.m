//
//  OdinConfigManager.m
//  OdinAnalysisSDK
//
//  Created by nathan on 2019/2/28.
//  Copyright © 2019 Odin. All rights reserved.
//

#import "OdinConfigManager.h"
#import "OdinConfig.h"

typedef void (^OdinNetworkComplteBlock)(void);
@interface OdinConfigManager()
@property(nonatomic,assign)OdinNetworkReachabilityStatus status;
@property(nonatomic,copy)OdinNetworkComplteBlock complteBlock;
@end
static OdinConfigManager *instance;
@implementation OdinConfigManager
+ (id)allocWithZone:(struct _NSZone *)zone
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [super allocWithZone:zone];
    });
    
    return instance;
}

+ (instancetype)shareInstance
{
    if (instance == nil) {
        instance = [[OdinConfigManager alloc] init];
    }
    
    return instance;
}

- (OdinConfig *)defaultConfig{
    if (_defaultConfig==nil) {
        _defaultConfig=[[OdinConfig alloc]init];
        _defaultConfig.backgroundLiveTime=30000;
        _defaultConfig.dataExpiretime=15;
        
        _defaultConfig.wifiStartCount=80;
        _defaultConfig.wifiBackgroundIntervalTime=30000;
        _defaultConfig.wifiBackgroundCount=50;
        _defaultConfig.wifiIntervalTime=30000;
        _defaultConfig.wifiIntervalCount=50;
        
        _defaultConfig.wwanStartCount=50;
        _defaultConfig.wwanBackgroundIntervalTime=30000;
        _defaultConfig.wwanBackgroundCount=30;
        _defaultConfig.wwanIntervalTime=30000;
        _defaultConfig.wwanIntervalCount=30;
        
    }
    return _defaultConfig;
}

- (void)startMonitoring:(void (^)(void))compelte{
     __weak __typeof(self)weakSelf = self;
    [[OdinNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(OdinNetworkReachabilityStatus status) {
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.complteBlock) {
            strongSelf.complteBlock();
        }
        strongSelf.status=status;;
    }];
    [[OdinNetworkReachabilityManager sharedManager] startMonitoring];
}

- (NSInteger)getSendCount:(OdinAppStatus)appStatus{
    OdinConfig *config=nil;
    if ([OdinConfigManager shareInstance].useConfig==nil) {
        //使用默认config
        config=[OdinConfigManager shareInstance].defaultConfig;
    }else{
        //使用 useConfig
        config=[OdinConfigManager shareInstance].useConfig;
    }
   
    return [self getSendCountByCongig:config appStatus:appStatus];
}

- (NSInteger)getTimeInterval:(OdinAppStatus)appStatus{
    OdinConfig *config=nil;
    if ([OdinConfigManager shareInstance].useConfig==nil) {
        //使用默认config
        config=[OdinConfigManager shareInstance].defaultConfig;
    }else{
        //使用 useConfig
        config=[OdinConfigManager shareInstance].useConfig;
    }
    
    return [self getTimerIntervalByCongig:config appStatus:appStatus];
}

-(NSString *)getWorkType{
    NSString *type=@"UNKNOW";
    switch (self.status) {
        case OdinNetworkReachabilityStatusUnknown:
            
            break;
            
        case OdinNetworkReachabilityStatusNotReachable:
            
            break;
            
//        case OdinNetworkReachabilityStatusReachableViaWWAN:
        case OdinNetworkReachabilityStatusReachableViaWWAN2G:
            type=@"2G";
            break;
        case OdinNetworkReachabilityStatusReachableViaWWAN3G:
            type=@"3G";
            break;
        case OdinNetworkReachabilityStatusReachableViaWWAN4G:
            type=@"4G";
            break;
        case OdinNetworkReachabilityStatusReachableViaWiFi:
            type=@"WIFI";
            break;
        default:
            break;
    }
    Odin_Debug(@"网络状态:%@",type);
    return type;
}

- (NSInteger)getSendCountByCongig:(OdinConfig *)conig appStatus:(OdinAppStatus)appStatus{
    Odin_Info(@"config:%ld",(long)conig.wifiStartCount);
    Odin_Info(@"ReachabilityStatus:%ld",(long)self.status);
     //进行网络监听，根据网络类型返回发送数量
    switch (self.status) {
        case OdinNetworkReachabilityStatusUnknown:
            return 0;
            break;
            
        case OdinNetworkReachabilityStatusNotReachable:
            return 0;
            break;
            
        case OdinNetworkReachabilityStatusReachableViaWWAN:
        case OdinNetworkReachabilityStatusReachableViaWWAN2G:
        case OdinNetworkReachabilityStatusReachableViaWWAN3G:
        case OdinNetworkReachabilityStatusReachableViaWWAN4G:
            if (appStatus==OdinAppStatusStart) {
                return self.useConfig.wwanStartCount;
            }else if (appStatus==OdinAppStatusUsing){
                return self.useConfig.wwanIntervalCount;
            }else if (appStatus==OdinAppStatusBackground){
                return self.useConfig.wwanBackgroundCount;
            }
            break;
            
        case OdinNetworkReachabilityStatusReachableViaWiFi:
            if (appStatus==OdinAppStatusStart) {
                return self.useConfig.wifiStartCount;
            }else if (appStatus==OdinAppStatusUsing){
                return self.useConfig.wifiIntervalCount;
            }else if (appStatus==OdinAppStatusBackground){
                return self.useConfig.wifiBackgroundCount;
            }
            break;
        default:
            break;
    }
    return 30;
}


- (NSInteger)getTimerIntervalByCongig:(OdinConfig *)conig appStatus:(OdinAppStatus)appStatus{
    switch (self.status) {
        case OdinNetworkReachabilityStatusUnknown:
            
            break;
            
        case OdinNetworkReachabilityStatusNotReachable:
            
            break;
            
        case OdinNetworkReachabilityStatusReachableViaWWAN:
        case OdinNetworkReachabilityStatusReachableViaWWAN2G:
        case OdinNetworkReachabilityStatusReachableViaWWAN3G:
        case OdinNetworkReachabilityStatusReachableViaWWAN4G:
             if (appStatus==OdinAppStatusUsing){
                return conig.wwanIntervalTime;
            }else if (appStatus==OdinAppStatusBackground){
                return conig.wwanBackgroundIntervalTime;
            }else{
                 return conig.wwanIntervalTime;
            }
            break;
            
        case OdinNetworkReachabilityStatusReachableViaWiFi:
             if (appStatus==OdinAppStatusUsing){
                return conig.wifiIntervalTime;
            }else if (appStatus==OdinAppStatusBackground){
                return conig.wifiBackgroundIntervalTime;
            }else{
                 return conig.wifiIntervalTime;
            }
            break;
        default:
            break;
    }
    return 30000;
}

@end
