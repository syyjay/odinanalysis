//
//  AodSendConfig.h
//  AodanalysisSDK
//
//  Created by nathan on 2019/2/26.
//  Copyright © 2019 Odin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN



@interface OdinConfig : NSObject
//后台停留时长作为新会话 单位秒
@property(nonatomic,assign) NSInteger backgroundLiveTime;
//本地数据库过期清理时长 单位天数
@property(nonatomic,assign) NSInteger dataExpiretime;

//数据发送频率/条数
@property(nonatomic,assign) NSInteger wifiBackgroundCount;
@property(nonatomic,assign) NSInteger wifiBackgroundIntervalTime;
@property(nonatomic,assign) NSInteger wifiIntervalCount;
@property(nonatomic,assign) NSInteger wifiIntervalTime;
@property(nonatomic,assign) NSInteger wifiStartCount;

@property(nonatomic,assign) NSInteger wwanBackgroundCount;
@property(nonatomic,assign) NSInteger wwanBackgroundIntervalTime;
@property(nonatomic,assign) NSInteger wwanIntervalCount;
@property(nonatomic,assign) NSInteger wwanIntervalTime;
@property(nonatomic,assign) NSInteger wwanStartCount;

@end

NS_ASSUME_NONNULL_END
