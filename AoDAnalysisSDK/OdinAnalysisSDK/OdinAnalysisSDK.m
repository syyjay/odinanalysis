//
//  AodanalysisSDK.m
//  AodanalysisSDK
//
//  Created by nathan on 2019/2/26.
//  Copyright © 2019 Odin. All rights reserved.
//

#import "OdinAnalysisSDK.h"
@implementation OdinAnalysisSDK
/*
 * 初始化SDK
 * @param   appkey      APP注册时获取唯一标识符
 * @param   channel     当前使用渠道，设置nil默认APP Store
 */
+ (void)initSDKWithAPPKey:(NSString *)appkey Channel:(NSString *)channel{
    //开启数据库
   BOOL success= [[OdinSqlLiteManager shareInstance] openDB];
    if (success) {
        NSLog(@"数据库打开成功");
    }
    [[OdinDataManager shareInstance].headDic setValue:appkey forKey:@"appkey"];
    [[OdinDataManager shareInstance].headDic setValue:channel?channel:@"AppStore" forKey:@"active_channel"];
    [[OdinDataManager shareInstance].headDic setValue:channel?channel:@"AppStore" forKey:@"channel"];
    
  
    dispatch_queue_t dispatchQueue =dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    dispatch_group_async(group, dispatchQueue, ^{
        
        //开始网络监听
        [[OdinConfigManager shareInstance]setComplteBlock:^{
              dispatch_group_leave(group);
            [[OdinConfigManager shareInstance]setComplteBlock:nil];
        }];
        [[OdinAnalysisManager shareInstance] startMonitoring:nil];
    });
    dispatch_group_enter(group);
    dispatch_group_async(group,dispatchQueue, ^{
        //发送策略
        [[OdinAnalysisManager shareInstance] loadConfig:^{
            dispatch_group_leave(group);
        }];
    });
    dispatch_group_enter(group);
    dispatch_group_async(group,dispatchQueue, ^{
        //时间校对
        [[OdinAnalysisManager shareInstance] checkTime:^{
            dispatch_group_leave(group);
        }];
    });

    //当所有的任务都完成后会发送这个通知
    dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
       //存入缓存数据
        NSMutableArray *cacheEvent=[NSMutableArray arrayWithContentsOfFile:[NSString aod_getCacheEventDataPath]];
        if (cacheEvent) {
            for (NSDictionary *eventDic in cacheEvent) {
                NSInteger type=[eventDic[@"eventType"] integerValue];
                [[OdinDataManager shareInstance]saveCacheEventData:type attributs:eventDic[@"attributs"]];
                
            }
        }
        
        //校正数据 网络状态和时间 sdkInitComplete 
        [[OdinSqlLiteManager shareInstance] selectKeys:@[@{@"id":Odin_SQL_DATATYPE_INTRGER},@{@"headInfo":Odin_SQL_DATATYPE_TEXT},@{@"bodyInfo":Odin_SQL_DATATYPE_TEXT},@{@"stat_time":Odin_SQL_DATATYPE_TEXT}] fromTable:@"AodEvenDataTb" orderBy:@"id" orderType:Odin_SQL_ORDERTYPE_DESC whereString:@"sdkInitComplete=0" limit:nil complete:^(NSArray * _Nonnull dataArr, NSError * _Nonnull error) {
            if (dataArr.count>0) {
                for (NSDictionary *eventDic in dataArr) {

                    NSString *envtId=eventDic[@"id"];
                    //headInfo
                    NSString *headInfoStr=eventDic[@"headInfo"];
                    NSString *bodyInfoStr=eventDic[@"bodyInfo"];
                    NSString *stat_timeStr=eventDic[@"stat_time"];

                    NSTimeInterval timeInterval=[OdinTimeTool timeSwitchTimestamp:stat_timeStr]+[OdinAnalysisManager shareInstance].diffTime;
                    NSString *stat_timeResultStr=[OdinTimeTool timestampSwitchTimestring:[NSDate dateWithTimeIntervalSince1970:timeInterval]];
                    //@"network_type"
                    NSDictionary *headInfoDic=[NSString aod_dictionaryWithJsonString:headInfoStr];
                    //@"stat_time"
                    NSDictionary *bodyInfoDic=[NSString aod_dictionaryWithJsonString:bodyInfoStr];

                    NSMutableDictionary *headInfoMDic=[NSMutableDictionary dictionaryWithDictionary:headInfoDic];
                    NSMutableDictionary *bodyInfoMDic=[NSMutableDictionary dictionaryWithDictionary:bodyInfoDic];
                    [headInfoMDic setObject:[OdinConfigManager shareInstance].getWorkType forKey:@"network_type"];
                    [bodyInfoMDic setObject:stat_timeResultStr forKey:@"stat_time"];

                    NSString *headInfoResultStr=[NSString aod_jsonString:headInfoMDic];
                    NSString *bodyInfoResultStr=[NSString aod_jsonString:bodyInfoMDic];
                    [[OdinSqlLiteManager shareInstance] updateData:@{@"headInfo":headInfoResultStr,@"bodyInfo":bodyInfoResultStr,@"stat_time":stat_timeResultStr,@"sdkInitComplete":[NSNumber numberWithInteger:1]} inTable:@"AodEvenDataTb" whereString:[NSString stringWithFormat:@"id=%@",envtId]];
                }
            }
            [OdinAnalysisManager shareInstance].sdkInitComplete=YES;
        }];
        //启动时发送一次数据
        [[OdinAnalysisManager shareInstance] sendDataByLauch];

        //首次启动事件监测
        [[OdinAnalysisManager shareInstance] firstLauch];

        //开启会话
        [[OdinSessionManager shareInstance] startSession:OdinAppStatusStart];

    });

    //页面追踪
    [UIViewController odin_swizzIt];
    //异常追踪
    [OdinErrorAnalysisHandler sharedHandler];
  
}

/*
 * 注册事件统计
 * @param   userId     用户id(用户注册成功后获取)
 */
+ (void)eventRegister:(NSString *)userId{
    [OdinDataManager shareInstance].headDic[@"user_id"]=userId;
    [[OdinDataManager shareInstance] saveEventData:OdinEventTypeRegister attributs:@{@"user_id":userId} ohterAttributs:nil complete:nil];
}

/*
 * 登录接口
 * @param   userId          用户id(用户登录成功后获取)
 * @param   state           登录状态;1成功0失败。
 */
+ (void)eventLogin:(NSString *)userId status:(NSInteger)status{
    [OdinDataManager shareInstance].headDic[@"user_id"]=userId;
    [[OdinDataManager shareInstance] saveEventData:OdinEventTypeLogin attributs:@{@"status":[NSString stringWithFormat:@"%ld",(long)status]} ohterAttributs:nil complete:nil];
    
}

/*
 * 开始登录统计（用于统计登录时长）
 */
+ (void)beginEventLogin{
    [[OdinDataManager shareInstance] saveEventData:OdinEventTypeLogin attributs:nil ohterAttributs:@{@"eventStatus":@"0"} complete:nil];
}

/*
 * 结束登录统计(配合开始登录使用)
 * @param   userId          用户id(用户登录成功后获取，失败传空)
 * @param   state           登录状态;1成功0失败。
 */
+ (void)endEventLogin:(NSString *)userId status:(NSInteger)status{
    [OdinDataManager shareInstance].headDic[@"user_id"]=userId;
    [[OdinDataManager shareInstance] saveEventData:OdinEventTypeLogin attributs:@{@"status":[NSString stringWithFormat:@"%ld",(long)status]} ohterAttributs:@{@"eventStatus":@"1"} complete:nil];
}

/*
 * 支付事件
 * @param   payAmount       支付金额
 * @param   orderNumber     订单编号
 */
+ (void)eventPay:(double)payAmount orderNumber:(NSString *)orderNumber amount:(NSString *)amount{
    [[OdinDataManager shareInstance] saveEventData:OdinEventTypePayment attributs:@{@"order_number":orderNumber,@"amount":amount} ohterAttributs:nil complete:nil];
}

/*
 * 操作事件统计（点击、双击、滑动等）
 * @param   eventId         事件名称
 */
+ (void)eventOperation:(NSString *)eventId{
    [[OdinDataManager shareInstance] saveEventData:OdinEventTypeOperation attributs:@{@"eventId":eventId}ohterAttributs:nil complete:nil];
}

/*
 * 自定义事件
 * @param   eventId       事件eventId 说明：不超过20位，必须小写英文字母加下划线，英文字母结尾，每个事件名称唯一
 * @param   attributes      自定义属性{"key1":"value1","key2":"value2"}
 */
+ (void)eventCustom:(NSString *)eventId attributes:(NSDictionary *)attributes{
    NSMutableDictionary *atts=[NSMutableDictionary dictionaryWithDictionary:attributes];
    atts[@"eventId"]=eventId;
    [[OdinDataManager shareInstance] saveEventData:OdinEventTypeCustom attributs:atts ohterAttributs:nil complete:nil];
}

/**
 自定定义事件时长统计开始
 
 @param eventId 自定义事件名称或者ID
 @param attributes 为自定义事件的属性和取值(@"key1":@"value1",@"key1":@"value1")
 */
+ (void)beginEventCustom:(NSString *)eventId attributes:(NSDictionary *)attributes{
    NSMutableDictionary *atts=[NSMutableDictionary dictionaryWithDictionary:attributes];
    atts[@"eventId"]=eventId;
    [[OdinDataManager shareInstance] saveEventData:OdinEventTypeCustom attributs:atts ohterAttributs:@{@"eventStatus":@"0"} complete:nil];
}

/**
 自定定义事件时长统计结束
 
 @param eventId 自定义事件名称或者ID
 @param attributes 为自定义事件的属性和取值(@"key1":@"value1",@"key1":@"value1")
 */
+ (void)endEventCustom:(NSString *)eventId attributes:(NSDictionary *)attributes{
    NSMutableDictionary *atts=[NSMutableDictionary dictionaryWithDictionary:attributes];
    atts[@"eventId"]=eventId;
    [[OdinDataManager shareInstance] saveEventData:OdinEventTypeCustom attributs:atts ohterAttributs:@{@"eventStatus":@"1"} complete:nil];
}


/**
 异常事件统计
 @param title 异常标题
 @param context 异常内容
 */
+ (void)eventErrorTitle:(NSString *)title context:(NSString *)context{
     [[OdinDataManager shareInstance] saveEventData:OdinEventTypeError attributs:@{@"title":title,@"context":context,@"category":@"2"} ohterAttributs:nil complete:nil];
}

/**
 针对PageView的统计
 
 @param pageName 开始页面的名称
 */
+ (void)pageStart:(NSString *)pageName{
    
    //    @"customPage": 0 页面开始  1 页面结束
    [[OdinDataManager shareInstance] saveEventData:OdinEventTypePageView attributs:@{@"page_id":pageName} ohterAttributs:@{@"pageStatus":@"0"} complete:nil];
}

/**
 针对PageView的统计
 
 @param pageName 结束页面的名称
 */
+ (void)pageEnd:(NSString *)pageName{
    [[OdinDataManager shareInstance] saveEventData:OdinEventTypePageView attributs:@{@"page_id":pageName} ohterAttributs:@{@"pageStatus":@"1"} complete:nil];
}

/*
 * 是否支持打印log信息
 * @param   canLog      默认NO，仅开发调试用，正式上传必须关闭
 */
+ (void)setEnableLog:(BOOL)enable{
    [[OdinLogHelper sharedInstance] setLogLevel:(enable?100:0)];
}

+ (void)getAllEventData:(void (^)(NSArray *dataArr))complete{
    [[OdinSqlLiteManager shareInstance] selectKeys:@[@{@"id":Odin_SQL_DATATYPE_INTRGER},@{@"headInfo":Odin_SQL_DATATYPE_TEXT},@{@"bodyInfo":Odin_SQL_DATATYPE_TEXT},@{@"type":Odin_SQL_DATATYPE_TEXT}] fromTable:@"AodEvenDataTb" orderBy:@"id" orderType:Odin_SQL_ORDERTYPE_DESC whereString:nil limit:nil complete:^(NSArray * _Nonnull dataArr, NSError * _Nonnull error) {
        if (dataArr.count>0) {
            if (complete) {
                complete(dataArr);
            }
        }else {
            if (complete) {
                complete(nil);
            }
        }
    }];
}

+ (NSString *)getDataSendPath{
  return   [[OdinLogHelper sharedInstance] valueForKeyPath:@"dataSendPath"];
}


@end
