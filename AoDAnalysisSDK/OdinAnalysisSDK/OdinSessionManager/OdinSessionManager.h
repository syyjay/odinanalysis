//
//  OdinSessionManager.h
//  OdinAnalysisSDK
//
//  Created by nathan on 2019/2/27.
//  Copyright © 2019 Odin. All rights reserved.
//

#import <Foundation/Foundation.h>
@class OdinSession;
NS_ASSUME_NONNULL_BEGIN

@interface OdinSessionManager : NSObject
+(instancetype)shareInstance;
@property(nonatomic,copy)NSString  *sessionStartTime;
@property(nonatomic,strong) OdinSession * _Nullable session;
- (void)resetSeesion;
- (void)startSession:(OdinAppStatus)appStatus;
@end

NS_ASSUME_NONNULL_END
