//
//  OdinSessionManager.m
//  OdinAnalysisSDK
//
//  Created by nathan on 2019/2/27.
//  Copyright © 2019 Odin. All rights reserved.
//

#import "OdinSessionManager.h"
#import "OdinSession.h"

@interface OdinSessionManager()
@property(nonatomic,assign) BOOL modifySysTime;
@property(nonatomic,copy)NSString  *sessionEndTime;
@property(nonatomic,strong)NSTimer *sessiionTimer;
@property(nonatomic,assign) BOOL backGround;
@property(nonatomic,copy) NSString *enterBackGroundTime;
@property(nonatomic,assign) BOOL resetSession;
@end

@implementation OdinSessionManager
static OdinSessionManager *instance;

- (NSTimer *)sessiionTimer{
    if (_sessiionTimer==nil) {
        _sessiionTimer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(sessiionTimerAction:) userInfo:nil repeats:YES];
     
    }
    return _sessiionTimer;
}

+ (id)allocWithZone:(struct _NSZone *)zone
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [super allocWithZone:zone];
        [instance appListen];
    });
    
    return instance;
}

+ (instancetype)shareInstance
{
    if (instance == nil) {
        instance = [[OdinSessionManager alloc] init];
        [instance initSession];
    }
    
    return instance;
}

- (void)resetSeesion{
    [self initSession];
    self.modifySysTime=NO;
}

- (void)startSession:(OdinAppStatus)appStatus{
    self.session.session_id=[self sessionId];
    self.sessionStartTime=[OdinTimeTool getCheckCurrentTime];
    if([OdinAnalysisManager shareInstance].sdkInitComplete==NO){
        //记录一次会话开始事件
        [[OdinDataManager shareInstance] saveEventData:OdinEventTypeSession attributs:@{@"sessionType":@"1"} ohterAttributs:@{@"sdkInitComplete":[NSNumber numberWithInteger:0]} complete:nil];
    }else{
        //记录一次会话开始事件
        [[OdinDataManager shareInstance] saveEventData:OdinEventTypeSession attributs:@{@"sessionType":@"1"} ohterAttributs:nil complete:nil];
    }
  
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        if ([UIApplication sharedApplication].applicationState!=UIApplicationStateBackground) {
            [[OdinAnalysisManager shareInstance] startAnalysis:appStatus];
        }
    }];
   
}

#pragma mark --private method
- (void)initSession{
    self.session=[[OdinSession alloc]init];
}

- (NSString *)sessionId{
    return [NSString aod_getMd5String:[[UIDevice aod_getDeviceId] stringByAppendingString:[OdinTimeTool getCheckCurrentTime:@"YYYYMMddHHmmssSSS"]]];
}

- (void)sessiionTimerAction:(NSTimer *)timer{
    if (self.backGround) {
        //当处于后台的时间 大于30秒时重置session
        NSTimeInterval difftTime=[OdinTimeTool getDiffTime:self.enterBackGroundTime endTime:[OdinTimeTool getCheckCurrentTime]];
        NSInteger liveTime=[OdinConfigManager shareInstance].useConfig.backgroundLiveTime/1000.0;
        if (liveTime<=0) {
            liveTime=30;
        }
        if (difftTime>liveTime) {
            //记录一次session结束事件 一次页面结束事件
            [[OdinDataManager shareInstance] saveEventData:OdinEventTypeSession attributs:@{@"sessionType":@"2",@"du":[NSNumber numberWithInteger:[OdinTimeTool getDiffTime:self.sessionStartTime endTime:[OdinTimeTool getLocalCurrentTime]]*1000]} ohterAttributs:@{@"duStartTime":self.sessionStartTime} complete:^(BOOL success) {
                
                UIViewController *currentVc=[[OdinErrorAnalysisHandler sharedHandler] getCurrentVC];
                NSArray *vcs=[currentVc.navigationController viewControllers];
                NSInteger index=[vcs indexOfObject:currentVc];
                UIViewController *lastVc;
                if (index>0) {
                    lastVc=vcs[index-1];
                }
                NSArray *pageArr=[OdinDataManager shareInstance].pageManagerArr;
                NSString *currentTimeString=[OdinTimeTool getCheckCurrentTime];
                [[OdinDataManager shareInstance] saveEventData:OdinEventTypePageView attributs:@{@"page_id":NSStringFromClass([currentVc class]),@"prepage_id":(lastVc? NSStringFromClass([lastVc class]):@"UNKNOWN"),@"du":[NSNumber numberWithInteger:[OdinTimeTool getDiffTime:[OdinDataManager shareInstance].viewAppearTimeStr endTime:currentTimeString]*1000]} ohterAttributs:@{@"duStartTime":[OdinDataManager shareInstance].viewAppearTimeStr} complete:^(BOOL success) {
                    if (pageArr.count>0) {
                        NSString *pageId=[pageArr.lastObject valueForKeyPath:@"pageId"];
                        NSString *prePageId=[pageArr.lastObject valueForKeyPath:@"prePageId"];
                        NSString * pageAppearTime=[pageArr.lastObject valueForKeyPath:@"pageAppearTime"];
                        //存字界面
                        [[OdinDataManager shareInstance] saveEventData:OdinEventTypePageView attributs:@{@"page_id":pageId,@"prepage_id":(prePageId? prePageId:@"UNKNOWN"),@"du":[NSNumber numberWithInteger:[OdinTimeTool getDiffTime:pageAppearTime endTime:currentTimeString]*1000]} ohterAttributs:@{@"duStartTime":pageAppearTime} complete:^(BOOL success) {
                            [self  resetSeesion];
                            self.resetSession=YES;
                            self.backGround=NO;
                        }];
                    }else{
                        [self  resetSeesion];
                        self.resetSession=YES;
                        self.backGround=NO;
                    }
                }];
            }];
        }
    }
}


- (void)pauseSessTimer{
    [self.sessiionTimer setFireDate:[NSDate distantFuture]];
}

- (void)resumeSessionTimer{
    [self.sessiionTimer setFireDate:[NSDate distantPast]];
}

- (void)stopSessionTimer{
    [self.sessiionTimer invalidate];
    self.sessiionTimer = nil;
}

/**
 移除所有通知
 */
- (void)remvoeAllObserver{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

- (void)userChangeClock:(NSNotification *)note{
    [OdinSessionManager shareInstance].modifySysTime=YES;
}

- (void)endSessionAction {
    if (self.resetSession==NO) {
        [OdinDataManager shareInstance].headDic[@"network_type"]=[[OdinConfigManager shareInstance] getWorkType];
        
        NSDictionary *sessionEndDic=@{@"eventType":[NSNumber numberWithUnsignedInteger:OdinEventTypeSession],
                                      @"attributs":@{@"sessionType":@"2",
                                                     @"du":[NSNumber numberWithInteger:[OdinTimeTool getDiffTime:self.sessionStartTime endTime:[OdinTimeTool getLocalCurrentTime]]*1000],
                                                     @"session_id":[OdinSessionManager shareInstance].session.session_id,
                                                     @"stat_time":[OdinTimeTool getCheckCurrentTime],
                                                     @"headInfo":[NSString aod_jsonString:[OdinDataManager shareInstance].headDic],
                                                     @"duStartTime":self.sessionStartTime
                                                     }
                                      
                                      };
        
        UIViewController *currentVc=[[OdinErrorAnalysisHandler sharedHandler] getCurrentVC];
        NSArray *vcs=[currentVc.navigationController viewControllers];
        NSInteger index=[vcs indexOfObject:currentVc];
        UIViewController *lastVc;
        if (index>0) {
            lastVc=vcs[index-1];
        }
        NSDictionary *pageEndDic=@{@"eventType":[NSNumber numberWithUnsignedInteger:OdinEventTypePageView],
                                   @"attributs":@{@"page_id":NSStringFromClass([currentVc class]),
                                                  @"prepage_id":(lastVc? NSStringFromClass([lastVc class]):@"UNKNOWN"),
                                                  @"du":[NSNumber numberWithInteger:[OdinTimeTool getDiffTime:[OdinDataManager shareInstance].viewAppearTimeStr endTime:[OdinTimeTool getCheckCurrentTime]]*1000],
                                                  @"session_id":[OdinSessionManager shareInstance].session.session_id,
                                                  @"stat_time":[OdinTimeTool getCheckCurrentTime],@"headInfo":[NSString aod_jsonString:[OdinDataManager shareInstance].headDic],
                                                  @"duStartTime":[OdinDataManager shareInstance].viewAppearTimeStr
                                                  }
                                   
                                   };
        
        NSMutableArray *eventArr=[NSMutableArray arrayWithArray:@[sessionEndDic,pageEndDic]];
        [eventArr writeToFile:[NSString aod_getCacheEventDataPath] atomically:YES];
        
    }
}

#pragma mark --APP状态切换
- (void)appListen{
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self
                           selector:@selector(aod_applicationWillEnterForeground:)
                               name:UIApplicationWillEnterForegroundNotification
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(aod_applicationDidBecomeActive:)
                               name:UIApplicationDidBecomeActiveNotification
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(aod_applicationWillResignActive:)
                               name:UIApplicationWillResignActiveNotification
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(aod_applicationDidEnterBackground:)
                               name:UIApplicationDidEnterBackgroundNotification
                             object:nil];
    
    [notificationCenter addObserver:self
                           selector:@selector(aod_applicationWillTerminateNotification:)
                               name:UIApplicationWillTerminateNotification
                             object:nil];
}


/**
 进入活跃状态的处理
 */
- (void)forGroundHandler {
    [self pauseSessTimer];
    
    [OdinAnalysisManager shareInstance].appStatus=OdinAppStatusUsing;
    if ([OdinSessionManager shareInstance].modifySysTime) {
        //重新校准时间
        [[OdinAnalysisManager shareInstance] checkTime:nil];
    }
    //移除系统修改时间的通知
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIApplicationSignificantTimeChangeNotification object:nil];
    
    if (self.resetSession) {
        //页面时间重置
        NSString *currentTimerStr=[OdinTimeTool getCheckCurrentTime];
        [OdinDataManager shareInstance].viewAppearTimeStr=currentTimerStr;
        NSArray *pageArr=[OdinDataManager shareInstance].pageManagerArr;
        if (pageArr.count>0) {
            [pageArr.lastObject setValue:currentTimerStr forKeyPath:@"pageAppearTime"];
        }
    
        [self startSession:OdinAppStatusUsing];
        self.resetSession=NO;
    }
    
    //开启活动
    [[OdinAnalysisManager shareInstance] startAnalysis:OdinAppStatusUsing];
}

//将要进入前台
- (void)aod_applicationWillEnterForeground:(NSNotification *)notification
{
 
}

//app活动状态
- (void)aod_applicationDidBecomeActive:(NSNotification *)notification
{
     [self forGroundHandler];
}

//当应用程序将要入非活动状态执行，在此期间，应用程序不接收消息或事件，比如来电话了
- (void)aod_applicationWillResignActive:(NSNotification *)notification
{
    [self backGroundHandler];
}


/**
 进入后台的处理
 */
- (void)backGroundHandler {
    self.enterBackGroundTime=[OdinTimeTool getCheckCurrentTime];
    self.backGround=YES;
    //暂停一切活动
    [[OdinAnalysisManager shareInstance] stopAnalysis];
    
    [OdinAnalysisManager shareInstance].appStatus=OdinAppStatusBackground;
    
    //开启活动
    [[OdinAnalysisManager shareInstance] startAnalysis:OdinAppStatusBackground];
    
    //监控修改系统时间
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(userChangeClock:) name:UIApplicationSignificantTimeChangeNotification object:nil];
    
//    //记录一次session结束事件、页面停留事件
//    [self endSessionAction];;
    
    [self resumeSessionTimer];
}

//app进入后台
- (void)aod_applicationDidEnterBackground:(NSNotification *)notification
{
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [[UIApplication sharedApplication]endBackgroundTask:UIBackgroundTaskInvalid];
    }];
}



//App死掉
- (void)aod_applicationWillTerminateNotification:(NSNotification *)notification
{
        //记录一次session结束事件、页面停留事件
    [self endSessionAction];
    
    [self stopSessionTimer];
    //清理定时器
    [[OdinAnalysisManager shareInstance] stopAnalysis];
    
    
}
@end
