//
//  OdinPageModel.h
//  OdinAnalysisSDK
//
//  Created by nathan on 2019/3/13.
//  Copyright © 2019 Odin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OdinPageModel : NSObject
@property(nonatomic,copy)NSString *pageId;
@property(nonatomic,copy)NSString *prePageId;
@property(nonatomic,copy)NSString *pageAppearTime;
@property(nonatomic,copy)NSString *pageDisappearTime;
@end

NS_ASSUME_NONNULL_END
