//
//  OdinDataManager.m
//  OdinAnalysisSDK
//
//  Created by nathan on 2019/3/1.
//  Copyright © 2019 Odin. All rights reserved.
//

#import "OdinDataManager.h"
#import "OdinGzipHelper.h"
#import "OdinPageModel.h"

@interface OdinDataManager()
@property(nonatomic,copy)NSString  *eventStartTime;

@end

@implementation OdinDataManager
aod_singleton_implementation(OdinDataManager)

#pragma mark --getter setter
- (NSMutableDictionary *)headDic{
    if (_headDic==nil) {
        _headDic=[NSMutableDictionary dictionary];
        _headDic[@"device_id"]=[UIDevice aod_getDeviceId];
        _headDic[@"sdk_version"]=OdinSDkVersion;
        _headDic[@"sdk_type"]=@"iOS";
        _headDic[@"user_id"]=@"";
        _headDic[@"app_version"]=[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        _headDic[@"carrier"]=[UIDevice aod_getCarrierInfomation];
        _headDic[@"os"]=[UIDevice currentDevice].systemName;
        _headDic[@"os_version"]=[UIDevice currentDevice].systemVersion;
        _headDic[@"screen_width"]=[NSString stringWithFormat:@"%.0f",[UIDevice aod_deviceSize].width*[UIScreen mainScreen].scale];
        _headDic[@"screen_height"]=[NSString stringWithFormat:@"%.0f",[UIDevice aod_deviceSize].height*[UIScreen mainScreen].scale];
        _headDic[@"brand"]=@"Apple";
        _headDic[@"device_model"]=[UIDevice aod_deviceModelName];
    }
    return _headDic;
}

- (NSMutableArray *)pageManagerArr{
    if (_pageManagerArr==nil) {
        _pageManagerArr=[NSMutableArray array];
    }
    return _pageManagerArr;
}


//手动pageView的统计
- (void)pageViewHandler:(OdinEventType)eventType extAttributs:(NSMutableDictionary *)extAttributs otherAttributs:(NSMutableDictionary * _Nullable)otherAttributs {
    
    NSString *pageStatus= otherAttributs[@"pageStatus"];
    dispatch_async(dispatch_get_main_queue(), ^{
        UIViewController *currentVc=[[OdinErrorAnalysisHandler sharedHandler] getCurrentVC];
        NSString *pageId=extAttributs[@"page_id"];
        //页面开始
        if (otherAttributs&&pageStatus&&pageStatus.integerValue==0) {
            pageId=[NSString stringWithFormat:@"%@/%@",NSStringFromClass([currentVc class]),pageId];
            OdinPageModel *page=[[OdinPageModel alloc]init];
            page.pageId=pageId;
            page.pageAppearTime=[OdinTimeTool getCheckCurrentTime];
            if (self.pageManagerArr.count==0) {
                NSArray  *viewControllers =currentVc.navigationController.viewControllers;
                NSInteger index=[viewControllers indexOfObject:currentVc];
                if (index>=1) {
                    UIViewController *lastVc=viewControllers[index-1];
                    page.prePageId=NSStringFromClass([lastVc class]);
                }
            }else{
                page.prePageId=[self.pageManagerArr.lastObject pageId];
            }
            [self.pageManagerArr addObject:page];
            
        }
        //页面结束
        if (otherAttributs&&pageStatus&&pageStatus.integerValue==1) {
            Odin_Info(@"attributs:%@",extAttributs);
            pageId=[NSString stringWithFormat:@"%@/%@",NSStringFromClass([currentVc class]),pageId];
            NSPredicate *predicate=[NSPredicate predicateWithFormat:@"pageId ENDSWITH %@",pageId];
            NSArray *filterArr= [self.pageManagerArr filteredArrayUsingPredicate:predicate];
            if (filterArr.count>0) {
                OdinPageModel *page=filterArr.lastObject;
                page.pageDisappearTime=[OdinTimeTool getCheckCurrentTime];
                //计算页面停留时长
                NSNumber *duStr=[NSNumber numberWithInteger:[OdinTimeTool getDiffTime:page.pageAppearTime endTime:page.pageDisappearTime]*1000];
                [extAttributs setObject:duStr forKey:@"du"];
                extAttributs[@"page_id"]=page.pageId;
                extAttributs[@"prepage_id"]=page.prePageId;
                [otherAttributs removeObjectForKey:@"pageStatus"];
                otherAttributs[@"duStartTime"]=page.pageAppearTime;
            }
        }
    });
}

- (void)saveEventData:(OdinEventType)eventType attributs:(NSDictionary *_Nullable)attributs ohterAttributs:(nullable NSDictionary *)otherAttributs complete:(nullable void (^)(BOOL success))complete{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSString *currentTimeString=[OdinTimeTool getCheckCurrentTime];
        NSMutableDictionary *eventDic=[NSMutableDictionary dictionary];
        [OdinDataManager shareInstance].headDic[@"network_type"]=[[OdinConfigManager shareInstance] getWorkType];
        eventDic[@"headInfo"]=[NSString aod_jsonString:[OdinDataManager shareInstance].headDic];
        NSMutableDictionary *bodyDic=[NSMutableDictionary dictionary];
        if ([OdinSessionManager shareInstance].session.session_id==nil) {
            return;
        }
     
    
        NSMutableDictionary *extAttributs=[NSMutableDictionary dictionaryWithDictionary:attributs];
        NSMutableDictionary *othertExtAttributs=[NSMutableDictionary dictionaryWithDictionary:otherAttributs];
        //手动pageView的统计
        [self pageViewHandler:eventType extAttributs:extAttributs otherAttributs:othertExtAttributs];
        NSString *pageStatus= othertExtAttributs[@"pageStatus"];
        //页面开始
        if (othertExtAttributs&&pageStatus&&(pageStatus.integerValue==0)) {
            return;
        }
        if (eventType==OdinEventTypePageView) {
            [bodyDic setValuesForKeysWithDictionary:@{@"session_id":[OdinSessionManager shareInstance].session.session_id,
                                                      @"stat_time":othertExtAttributs[@"duStartTime"]}];
            
        }else{
            [bodyDic setValuesForKeysWithDictionary:@{@"session_id":[OdinSessionManager shareInstance].session.session_id,
                                                      @"stat_time":currentTimeString}];
        }
        
        
        //其他事件需要统计时长的处理
        if (othertExtAttributs) {
            NSString *eventStatus=othertExtAttributs[@"eventStatus"];
            if (eventStatus) {
                if (eventStatus.integerValue==0) {
                    //开始
                    [OdinDataManager shareInstance].eventStartTime=[OdinTimeTool getLocalCurrentTime];
                }else{
                    //结束
                    NSNumber *duStr=[NSNumber numberWithInteger:[OdinTimeTool getDiffTime:[OdinDataManager shareInstance].eventStartTime endTime:currentTimeString]*1000];
                    [extAttributs setObject:duStr forKey:@"du"];
                }
            }
            if ([othertExtAttributs.allKeys containsObject:@"sdkInitComplete"]) {
                [eventDic setValuesForKeysWithDictionary:othertExtAttributs];
            }
            if ([othertExtAttributs.allKeys containsObject:@"duStartTime"]) {
                [eventDic setObject:othertExtAttributs[@"duStartTime"] forKey:@"duStartTime"];
            }
        }
        
        if (extAttributs) {
            
            if (eventType==OdinEventTypeOperation) {
                [bodyDic setValue:extAttributs[@"eventId"] forKey:@"id"];//body的额外属性
            }else{
                [bodyDic setValuesForKeysWithDictionary:extAttributs];//body的额外属性
            }
            
            if (eventType!=OdinEventTypeCustom) {
                [eventDic setValuesForKeysWithDictionary:extAttributs];//将额为属性存到数据库中，自定义事件属性不确定不存到数据库对应字段
            }
        }
        
        //bodyInfo
        eventDic[@"bodyInfo"]=[NSString aod_jsonString:bodyDic];
        eventDic[@"session_id"]=[OdinSessionManager shareInstance].session.session_id ;
        if (eventType==OdinEventTypeCustom) {
            eventDic[@"type"]=attributs[@"eventId"];
        }else{
            eventDic[@"type"]=[self getEventType:eventType attributs:extAttributs];
        }
        
        eventDic[@"stat_time"]=currentTimeString;
        BOOL success= [[OdinSqlLiteManager shareInstance] insertData:eventDic intoTable:@"AodEvenDataTb"];
        if (complete) {
            complete(success);
        }
    });
}


- (void)saveCacheEventData:(OdinEventType)eventType attributs:(NSDictionary *_Nullable)attributs{
    NSMutableDictionary *eventDic=[NSMutableDictionary dictionary];
    eventDic[@"headInfo"]=attributs[@"headInfo"];
    NSMutableDictionary *bodyDic=[NSMutableDictionary dictionary];
    bodyDic[@"session_id"]=attributs[@"session_id"];
    bodyDic[@"stat_time"]=attributs[@"stat_time"];
    eventDic[@"session_id"]=attributs[@"session_id"];
    eventDic[@"stat_time"]=attributs[@"stat_time"];
    eventDic[@"duStartTime"]=attributs[@"duStartTime"];
    eventDic[@"type"]=[self getEventType:eventType attributs:attributs];
    if (eventType==OdinEventTypeSession) {
        eventDic[@"sessionType"]=attributs[@"sessionType"];
        bodyDic[@"sessionType"]=attributs[@"sessionType"];
    }else{
        eventDic[@"page_id"]=attributs[@"page_id"];
        bodyDic[@"page_id"]=attributs[@"page_id"];
        
        eventDic[@"prepage_id"]=attributs[@"prepage_id"];
        bodyDic[@"prepage_id"]=attributs[@"prepage_id"];
    }
    eventDic[@"du"]=attributs[@"du"];
    
    bodyDic[@"du"]=attributs[@"du"];
    eventDic[@"bodyInfo"]=[NSString aod_jsonString:bodyDic];
    NSLog(@"cache:%@",eventDic);
    [[OdinSqlLiteManager shareInstance] insertData:eventDic intoTable:@"AodEvenDataTb"];
}

- (NSString *)sendDataString:(NSArray *)array{
    //@[{@"head":@"",@"body":@""}]
    NSMutableArray *sendArr=[NSMutableArray array];
    NSMutableArray *sameHeadData=[self getSameHeadData:array];//@[@[],@[],@[]] 每个元素分别为一种Head类型集合
    //处理相同type
    for (NSMutableArray *sendDataItem in sameHeadData) {
        //一条head 和body 数据
        NSMutableDictionary *resultDic=[NSMutableDictionary dictionary];
        [resultDic setObject:[NSString aod_dictionaryWithJsonString:sendDataItem.firstObject[@"headInfo"]] forKey:@"head"];
        NSMutableArray *bodyArr=[NSMutableArray array];
        NSMutableArray *sampTypeArr=[self getSameTypeData:sendDataItem];//@[@[],@[],@[]] 每个元素分别为一种type类型集合
        for (NSArray *typeItems in sampTypeArr) {
            NSMutableDictionary *bodyDic=[NSMutableDictionary dictionary];
            bodyDic[@"type"]=typeItems.firstObject[@"type"];
            NSMutableArray *bodyInfoArr=[typeItems valueForKeyPath:@"bodyInfo"];
            NSMutableArray *bodyInfoTempArr=[NSMutableArray array];
            for (NSString *bodyStr in bodyInfoArr) {
                [bodyInfoTempArr addObject:[NSString aod_dictionaryWithJsonString:bodyStr]];
            }
            bodyDic[@"data"]=bodyInfoTempArr;
            [bodyArr addObject:bodyDic];
        }
        [resultDic setObject:bodyArr forKey:@"body"];
        [sendArr addObject:resultDic];
    }
    NSString *sendString=[NSString aod_jsonString:sendArr];
    NSData *zippedData = [OdinGzipHelper gzipData:[sendString dataUsingEncoding:NSUTF8StringEncoding]];
    sendString = [zippedData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    return sendString;
}

#pragma mark --private method

- (NSString *)getEventType:(OdinEventType)eventType attributs:(NSDictionary *)attributs{
    NSString *typeString=@"";
    switch (eventType) {
        case OdinEventTypeVisitor:
            typeString=@"visitor";
            break;
        case OdinEventTypeRegister:
            typeString=@"user_register";
            break;
        case OdinEventTypeLogin:
            typeString=@"login";
            break;
        case OdinEventTypeSession:
            typeString=@"session";
            break;
        case OdinEventTypePageView:
            typeString=@"page_view";
            break;
        case OdinEventTypeOperation:
            typeString=@"operation";
            break;
        case OdinEventTypePayment:
            typeString=@"payment";
            break;
        case OdinEventTypeError:
            typeString=@"error";
            break;
        case OdinEventTypeCustom:
            typeString=attributs[@"eventId"];
            break;
            
        default:
            break;
    }
    return typeString;
}

/**
 获取相同头部信息的数据

 @param array 传入的数据
 @return 返回的数据
 */
- (NSMutableArray *)getSameHeadData:(NSArray *)array{
    NSMutableArray *headInfoArr=[array valueForKeyPath:@"headInfo"];
    //headInfoArr 去重
    NSMutableArray *headInfoResutlArr=[NSMutableArray array];
    for (NSString *headStr in headInfoArr) {
        if (![headInfoResutlArr containsObject:headStr]) {
            [headInfoResutlArr addObject:headStr];
        }
    }

    //计算headInfo 种类
    NSMutableArray *sendData_headArr=[NSMutableArray array];
    for (NSString *headInfo in headInfoResutlArr) {
        NSMutableArray *headInfo_envetData=[NSMutableArray array];
        for (NSDictionary *eventDataDic in array) {
            NSString *headStr=eventDataDic[@"headInfo"];
            if ([headInfo isEqualToString:headStr]) {
                [headInfo_envetData addObject:eventDataDic];
            }
        }
        [sendData_headArr addObject:headInfo_envetData];
    }
    return sendData_headArr;
}

/**
 获取相同type的数据
 
 @param array 传入的数据
 @return 返回的数据
 */
- (NSMutableArray *)getSameTypeData:(NSArray *)array{
    NSMutableArray *typeArr=[array valueForKeyPath:@"type"];
    //typeArr 去重
    NSMutableArray *typeResultArr=[NSMutableArray array];
    for (NSString *typeStr in typeArr) {
        if (![typeResultArr containsObject:typeStr]) {
            [typeResultArr addObject:typeStr];
        }
    }
    
    //计算type 种类
    NSMutableArray *sendData_typeArr=[NSMutableArray array];
    for (NSString *typeItem in typeResultArr) {
        NSMutableArray *type_envetData=[NSMutableArray array];
        for (NSDictionary *eventDataDic in array) {
            NSString *typeStr=eventDataDic[@"type"];
            if ([typeItem isEqualToString:typeStr]) {
                [type_envetData addObject:eventDataDic];
            }
        }
        [sendData_typeArr addObject:type_envetData];
    }
    return sendData_typeArr;
}

@end
