//
//  OdinDataManager.h
//  OdinAnalysisSDK
//
//  Created by nathan on 2019/3/1.
//  Copyright © 2019 Odin. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    OdinEventTypeVisitor,
    OdinEventTypeRegister,
    OdinEventTypeLogin,
    OdinEventTypeSession,
    OdinEventTypePageView,
    OdinEventTypeOperation,
    OdinEventTypePayment,
    OdinEventTypeError,
    OdinEventTypeCustom,
} OdinEventType;

NS_ASSUME_NONNULL_BEGIN

@interface OdinDataManager : NSObject
+(instancetype)shareInstance;
@property(nonatomic,strong)NSMutableArray *pageManagerArr;
/**
 头部信息
 */
@property(nonatomic,strong)NSMutableDictionary *headDic;
/**
 body信息，赋值前请先设置nil
 */
@property(nonatomic,strong)NSMutableDictionary *bodyDic;

/**
 保存事件

 @param eventType 事件类型
 @param attributs 事件额外的属性
  @param ohterAttributs 其他属性用于存放到数据库
 */
- (void)saveEventData:(OdinEventType)eventType attributs:(NSDictionary *_Nullable)attributs ohterAttributs:(nullable NSDictionary *)ohterAttributs complete:(nullable void (^)(BOOL success))complete;
- (void)saveCacheEventData:(OdinEventType)eventType attributs:(NSDictionary *_Nullable)attributs;
/**
 获取最终发送到服务器的 字符串数据

 @param array 传入的要发送的数组
 @return 返回最终生成的字符串
 */
- (NSString *)sendDataString:(NSArray *)array;

@property(nonatomic,copy)NSString *viewAppearTimeStr;
@property(nonatomic,strong)UIViewController *lastVc;
@end

NS_ASSUME_NONNULL_END
