//
//  NSObject+OdinExtension.h
//  AoDAnalysisSDK
//
//  Created by nathan on 2019/2/28.
//  Copyright © 2019 AoD. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (OdinExtension)

/**
 字典转模型

 @param dict 传入字段
 @return 对应模型
 */
+ (id)aod_modelWithDict:(NSDictionary *)dict;


/**
 模型转字典

 @param model 传入的模型
 @return 对应字典
 */
+ (id)aod_keyValues:(id)model;


/**
 json字符串转模型

 @param json json字符串
 @return 对应的模型
 */
+ (id)aod_modelWithJson:(NSString *)json;
@end

NS_ASSUME_NONNULL_END
