//
//  NSString+AoD.h
//  AoDAnalysisSDK
//
//  Created by nathan on 2019/2/27.
//  Copyright © 2019 AoD. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Odin)
+ (NSString *)aod_getMd5String:(NSString *)string;
+ (NSString *)aod_jsonString:(id )dics;

/**
 json字符串转字典

 @param jsonString 传入的json字符串
 @return 对应的字典
 */
+ (NSDictionary *)aod_dictionaryWithJsonString:(NSString *)jsonString;


+ (NSString *)aod_getCacheEventDataPath;
@end

NS_ASSUME_NONNULL_END
