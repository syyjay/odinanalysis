//
//  UIDevice+AoDAnalysis.h
//  AoDAnalysisSDK
//
//  Created by nathan on 2019/2/27.
//  Copyright © 2019 AoD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIDevice (OdinAnalysis)

/**
 获取设备ID
 */
+ (NSString *)aod_getDeviceId;

/**
 获取设备运营商

 @return 运营商名称
 */
+ (NSString *)aod_getCarrierInfomation;


/**
 手机尺寸

 @return 手机的尺寸
 */
+ (CGSize)aod_deviceSize;


/**
 设备类型

 @return 设备名称
 */
+ (NSString*)aod_deviceModelName;


/**
 是否首次启动app

 @return 是否首次启动app
 */
+ (BOOL)aod_firstLaunch;
@end

NS_ASSUME_NONNULL_END
