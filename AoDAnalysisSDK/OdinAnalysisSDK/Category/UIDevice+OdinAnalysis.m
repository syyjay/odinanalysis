//
//  UIDevice+AoDAnalysis.m
//  AoDAnalysisSDK
//
//  Created by nathan on 2019/2/27.
//  Copyright © 2019 AoD. All rights reserved.
//

#import "UIDevice+OdinAnalysis.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <AdSupport/ASIdentifierManager.h>
#import <sys/utsname.h>
#import <CoreTelephony/CTCarrier.h>

#define AoDBUNDLE_ID       [[NSBundle mainBundle] bundleIdentifier]
#define AoDDevice_Id       @"AoDDevice_Id"

@implementation UIDevice (OdinAnalysis)
+ (NSString *)aod_getDeviceId
{
    //从本地沙盒取
    NSString *uqid =[[NSUserDefaults standardUserDefaults] objectForKey:AoDDevice_Id];
   
    if (!uqid) {
        //从keychain取
        uqid = (NSString *)[self readObjectForKey:AoDDevice_Id];
        
        if (uqid) {
            [[NSUserDefaults standardUserDefaults] setObject:uqid forKey:AoDDevice_Id];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        } else {
            //从pasteboard取
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            id data = [pasteboard dataForPasteboardType:AoDDevice_Id];
            if (data) {
                uqid = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            }
            
            if (uqid) {
                [[NSUserDefaults standardUserDefaults] setObject:uqid forKey:AoDDevice_Id];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self saveObject:uqid forKey:AoDDevice_Id];
                
            } else {
                //获取idfa
                uqid = [self getIDFA];
                
                //idfa获取失败的情况，获取idfv
                if (!uqid || [uqid isEqualToString:@"00000000-0000-0000-0000-000000000000"]) {
                    uqid = [self getIDFV];
                    
                    //idfv获取失败的情况，获取uuid
                    if (!uqid) {
                        uqid = [self getUUID];
                    }
                }
                
                [[NSUserDefaults standardUserDefaults] setObject:uqid forKey:AoDDevice_Id];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self saveObject:uqid forKey:AoDDevice_Id];
                
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                NSData *data = [uqid dataUsingEncoding:NSUTF8StringEncoding];
                [pasteboard setData:data forPasteboardType:AoDDevice_Id];
                
            }
        }
    }
    return uqid;
}

+ (NSString *)aod_getCarrierInfomation {
    CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [info subscriberCellularProvider];
    if (carrier == nil) {
        return @"不能识别";
    }
    if (!carrier.isoCountryCode) {
//        AoD_Debug(@"没有SIM卡");
        return @"无运营商";
    }else{
        
        return [carrier carrierName];
    }
}

+ (CGSize)aod_deviceSize{
    return [UIScreen mainScreen].bounds.size;
}

+ (NSString*)aod_deviceModelName
{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *deviceModel = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    if ([deviceModel isEqualToString:@"iPhone1,1"])    return @"iPhone1";
    if ([deviceModel isEqualToString:@"iPhone1,2"])    return @"iPhone3G";
    if ([deviceModel isEqualToString:@"iPhone2,1"])    return @"iPhone3GS";
    if ([deviceModel isEqualToString:@"iPhone3,1"])    return @"iPhone4";
    if ([deviceModel isEqualToString:@"iPhone3,2"])    return @"iPhone4";
    if ([deviceModel isEqualToString:@"iPhone3,3"])    return @"iPhone4";
    if ([deviceModel isEqualToString:@"iPhone4,1"])    return @"iPhone4S";
    if ([deviceModel isEqualToString:@"iPhone5,1"])    return @"iPhone5";
    if ([deviceModel isEqualToString:@"iPhone5,2"])    return @"iPhone5";
    if ([deviceModel isEqualToString:@"iPhone5,3"])    return @"iPhone5c";
    if ([deviceModel isEqualToString:@"iPhone5,4"])    return @"iPhone5c";
    if ([deviceModel isEqualToString:@"iPhone6,1"])    return @"iPhone5s";
    if ([deviceModel isEqualToString:@"iPhone6,2"])    return @"iPhone5s";
    if ([deviceModel isEqualToString:@"iPhone7,1"])    return @"iPhone6 Plus";
    if ([deviceModel isEqualToString:@"iPhone7,2"])    return @"iPhone6";
    if ([deviceModel isEqualToString:@"iPhone8,1"])    return @"iPhone6s";
    if ([deviceModel isEqualToString:@"iPhone8,2"])    return @"iPhone6s Plus";
    if ([deviceModel isEqualToString:@"iPhone8,4"])    return @"iPhoneSE";
    if ([deviceModel isEqualToString:@"iPhone9,1"])    return @"iPhone7";
    if ([deviceModel isEqualToString:@"iPhone9,2"])    return @"iPhone7 Plus";
    if ([deviceModel isEqualToString:@"iPhone9,3"])    return @"iPhone7";
    if ([deviceModel isEqualToString:@"iPhone9,4"])    return @"iPhone7 Plus";
    if ([deviceModel isEqualToString:@"iPhone10,1"])   return @"iPhone8";
    if ([deviceModel isEqualToString:@"iPhone10,4"])   return @"iPhone8";
    if ([deviceModel isEqualToString:@"iPhone10,2"])   return @"iPhone8 Plus";
    if ([deviceModel isEqualToString:@"iPhone10,5"])   return @"iPhone8 Plus";
    if ([deviceModel isEqualToString:@"iPhone10,3"])   return @"iPhoneX";
    if ([deviceModel isEqualToString:@"iPhone10,6"])   return @"iPhoneX";
    if ([deviceModel isEqualToString:@"iPhone11,8"])   return @"iPhoneXR";
    if ([deviceModel isEqualToString:@"iPhone11,2"])   return @"iPhoneXS";
    if ([deviceModel isEqualToString:@"iPhone11,4"])   return @"iPhoneXS Max";
    if ([deviceModel isEqualToString:@"iPhone11,6"])   return @"iPhoneXS Max";
    if ([deviceModel isEqualToString:@"iPod1,1"])      return @"iPod Touch";
    if ([deviceModel isEqualToString:@"iPod2,1"])      return @"iPod Touch";
    if ([deviceModel isEqualToString:@"iPod3,1"])      return @"iPod Touch";
    if ([deviceModel isEqualToString:@"iPod4,1"])      return @"iPod Touch";
    if ([deviceModel isEqualToString:@"iPod5,1"])      return @"iPod Touch";
    if ([deviceModel isEqualToString:@"iPod7,1"])      return @"iPod Touch";
    if ([deviceModel isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([deviceModel isEqualToString:@"iPad2,1"])      return @"iPad2";
    if ([deviceModel isEqualToString:@"iPad2,2"])      return @"iPad2";
    if ([deviceModel isEqualToString:@"iPad2,3"])      return @"iPad2";
    if ([deviceModel isEqualToString:@"iPad2,4"])      return @"iPad2";
    if ([deviceModel isEqualToString:@"iPad2,5"])      return @"iPad Mini";
    if ([deviceModel isEqualToString:@"iPad2,6"])      return @"iPad Mini";
    if ([deviceModel isEqualToString:@"iPad2,7"])      return @"iPad Mini";
    if ([deviceModel isEqualToString:@"iPad3,1"])      return @"iPad3";
    if ([deviceModel isEqualToString:@"iPad3,2"])      return @"iPad3";
    if ([deviceModel isEqualToString:@"iPad3,3"])      return @"iPad3";
    if ([deviceModel isEqualToString:@"iPad3,4"])      return @"iPad4";
    if ([deviceModel isEqualToString:@"iPad3,5"])      return @"iPad4";
    if ([deviceModel isEqualToString:@"iPad3,6"])      return @"iPad4";
    if ([deviceModel isEqualToString:@"iPad4,1"])      return @"iPad Air";
    if ([deviceModel isEqualToString:@"iPad4,2"])      return @"iPad Air";
    if ([deviceModel isEqualToString:@"iPad4,3"])      return @"iPad Air";
    if ([deviceModel isEqualToString:@"iPad4,4"])      return @"iPad Mini2";
    if ([deviceModel isEqualToString:@"iPad4,5"])      return @"iPad Mini2";
    if ([deviceModel isEqualToString:@"iPad4,6"])      return @"iPad Mini2";
    if ([deviceModel isEqualToString:@"iPad4,7"])      return @"iPad Mini3";
    if ([deviceModel isEqualToString:@"iPad4,8"])      return @"iPad Mini3";
    if ([deviceModel isEqualToString:@"iPad4,9"])      return @"iPad Mini3";
    if ([deviceModel isEqualToString:@"iPad5,1"])      return @"iPad Mini4";
    if ([deviceModel isEqualToString:@"iPad5,2"])      return @"iPad Mini4";
    if ([deviceModel isEqualToString:@"iPad5,3"])      return @"iPad Air2";
    if ([deviceModel isEqualToString:@"iPad5,4"])      return @"iPad Air2";
    if ([deviceModel isEqualToString:@"iPad6,3"])      return @"iPad Pro9.7";
    if ([deviceModel isEqualToString:@"iPad6,4"])      return @"iPad Pro9.7";
    if ([deviceModel isEqualToString:@"iPad6,7"])      return @"iPad Pro12.9";
    if ([deviceModel isEqualToString:@"iPad6,8"])      return @"iPad Pro12.9";
    if ([deviceModel isEqualToString:@"iPad6,11"])     return @"iPad5";
    if ([deviceModel isEqualToString:@"iPad6,12"])     return @"iPad5";
    if ([deviceModel isEqualToString:@"iPad7,1"])      return @"iPad Pro12.9 (2nd generation)";
    if ([deviceModel isEqualToString:@"iPad7,2"])      return @"iPad Pro12.9 (2nd generation)";
    if ([deviceModel isEqualToString:@"iPad7,3"])      return @"iPad Pro10.5";
    if ([deviceModel isEqualToString:@"iPad7,4"])      return @"iPad Pro10.5";
    if ([deviceModel isEqualToString:@"iPad7,5"])      return @"iPad6";
    if ([deviceModel isEqualToString:@"iPad7,6"])      return @"iPad6";
    if ([deviceModel isEqualToString:@"iPad8,1"])      return @"iPad Pro11";
    if ([deviceModel isEqualToString:@"iPad8,2"])      return @"iPad Pro11";
    if ([deviceModel isEqualToString:@"iPad8,3"])      return @"iPad Pro11";
    if ([deviceModel isEqualToString:@"iPad8,4"])      return @"iPad Pro11";
    if ([deviceModel isEqualToString:@"iPad8,5"])      return @"iPad Pro12.9 (3nd generation)";
    if ([deviceModel isEqualToString:@"iPad8,6"])      return @"iPad Pro12.9 (3nd generation)";
    if ([deviceModel isEqualToString:@"iPad8,7"])      return @"iPad Pro12.9 (3nd generation)";
    if ([deviceModel isEqualToString:@"iPad8,8"])      return @"iPad Pro12.9 (3nd generation)";
    if ([deviceModel isEqualToString:@"i386"])         return @"Simulator";
    if ([deviceModel isEqualToString:@"x86_64"])       return @"Simulator";
    return deviceModel;
}

+ (BOOL)aod_firstLaunch
{
    //从本地沙盒取
    NSString *uqid = [[NSUserDefaults standardUserDefaults] objectForKey:AoDDevice_Id];
    if (!uqid) {
        //从keychain取
        uqid = (NSString *)[self readObjectForKey:AoDDevice_Id];
        if (uqid) {
            return NO;
        } else {
            //从pasteboard取
            UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
            id data = [pasteboard dataForPasteboardType:AoDDevice_Id];
            if (data) {
                return NO;
            }
            return YES;
        }
    }
    return NO;
}

#pragma mark --private method
//获取IDFA
+ (NSString *)getIDFA
{
    NSString *idfa = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    idfa = [idfa stringByReplacingOccurrencesOfString:@"-" withString:@""];
    return idfa;
}

//获取IDFV
+ (NSString *)getIDFV
{
    NSString *idfv = [[UIDevice currentDevice].identifierForVendor UUIDString];
    idfv = [idfv stringByReplacingOccurrencesOfString:@"-" withString:@""];
    return idfv;
}

//获取UUID
+ (NSString *)getUUID
{
    NSString *uuid = [[NSUUID UUID] UUIDString];
    uuid = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
    return uuid;
}

//////////////////keyChain 操作////////////
+ (void)saveObject:(id)object forKey:(NSString *)key {
    NSMutableDictionary *mDic = [NSMutableDictionary dictionary];
    [mDic setValuesForKeysWithDictionary:(NSMutableDictionary *)[self load:AoDBUNDLE_ID]];
    [mDic setObject:object forKey:key];
    [self save:AoDBUNDLE_ID data:mDic];
}

+ (id)readObjectForKey:(NSString *)key {
    NSMutableDictionary *mDic = (NSMutableDictionary *)[self load:AoDBUNDLE_ID];
    return [mDic objectForKey:key];
}

+ (id)load:(NSString *)service {
    id ret = nil;
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:service];
    //Configure the search setting
    [keychainQuery setObject:(id)kCFBooleanTrue forKey:(__bridge_transfer id)kSecReturnData];
    [keychainQuery setObject:(__bridge_transfer id)kSecMatchLimitOne forKey:(__bridge_transfer id)kSecMatchLimit];
    CFDataRef keyData = NULL;
    if (SecItemCopyMatching((__bridge_retained CFDictionaryRef)keychainQuery, (CFTypeRef *)&keyData) == noErr) {
        @try {
            ret = [NSKeyedUnarchiver unarchiveObjectWithData:(__bridge_transfer NSData *)keyData];
        } @catch (NSException *e) {
//            AoD_Debug(@"Unarchive of %@ failed: %@", service, e);
        } @finally {
        }
    }
    return ret;
}

+ (NSMutableDictionary *)getKeychainQuery:(NSString *)service {
    return [NSMutableDictionary dictionaryWithObjectsAndKeys:
            (__bridge_transfer id)kSecClassGenericPassword,(__bridge_transfer id)kSecClass,
            service, (__bridge_transfer id)kSecAttrService,
            service, (__bridge_transfer id)kSecAttrAccount,
            (__bridge_transfer id)kSecAttrAccessibleAfterFirstUnlock,(__bridge_transfer id)kSecAttrAccessible,
            nil];
}

+ (void)save:(NSString *)service data:(id)data {
    //Get search dictionary
    NSMutableDictionary *keychainQuery = [self getKeychainQuery:service];
    //Delete old item before add new item
    SecItemDelete((__bridge_retained CFDictionaryRef)keychainQuery);
    //Add new object to search dictionary(Attention:the data format)
    [keychainQuery setObject:[NSKeyedArchiver archivedDataWithRootObject:data] forKey:(__bridge_transfer id)kSecValueData];
    //Add item to keychain with the search dictionary
    SecItemAdd((__bridge_retained CFDictionaryRef)keychainQuery, NULL);
}
//////////////////keyChain 操作////////////
@end
