//
//  OdinAnalysisManager.m
//  OdinAnalysisSDK
//
//  Created by nathan on 2019/2/27.
//  Copyright © 2019 Odin. All rights reserved.
//

#import "OdinAnalysisManager.h"
#import "OdinConfig/OdinConfigManager.h"
#import "OdinConfig/OdinConfig.h"
#import "OdinSessionManager/OdinSessionManager.h"

@interface OdinAnalysisManager ()
@property(nonatomic,strong)NSTimer *timer;

@end

@implementation OdinAnalysisManager

static OdinAnalysisManager *instance;

aod_singleton_implementation(OdinAnalysisManager)

- (void)loadConfig:(void (^)(void))compelte{
    //服务获取成功使用服务器获取的并存本地，服务器获取失败使用本地的，本地没有使用默认策略
     [self loadServerConfig:compelte];
}

- (void)checkTime:(nullable void (^)(void))compete{
    NSString *url=[NSString stringWithFormat:@"%@%@",OdinCommonLinkUrl,OdinGetNetDate];
    [OdinHttp post:url parameters:nil success:^(id  _Nullable responseObject) {
        NSString *netTime=responseObject[@"netTime"];
        Odin_Debug(@"网络时间:%@",netTime);
        NSTimeInterval diffTime=[OdinTimeTool getDiffTime:[OdinTimeTool getLocalCurrentTime] endTime:netTime];
        if (ABS(diffTime)>1*60*60) {//当有时区差时才去校正时间 以一个小时为判断标志
            [OdinAnalysisManager shareInstance].diffTime=[OdinTimeTool getDiffTime:[OdinTimeTool getLocalCurrentTime] endTime:netTime];
        }
        if (compete) {
            compete();
        }
    } failure:^(NSError * _Nonnull error) {
        Odin_Error(@"获取网络时间失败");
        if (compete) {
            compete();
        }
    }];
}

- (void)firstLauch{
    if ([UIDevice aod_firstLaunch]) {
        //存入 首次启动事件
        [[OdinDataManager shareInstance] saveEventData:OdinEventTypeVisitor attributs:nil ohterAttributs:nil complete:nil];
    }
}

/**
 开始收集数据
 */
- (void)startAnalysis:(OdinAppStatus)appStatus{
    [self startTimer:appStatus];
}

/**
 停止收集数据
 */
- (void)stopAnalysis{
    [self stopTimer];
}

- (void)startMonitoring:(nullable void (^)(void))compelte{
    [[OdinConfigManager shareInstance] startMonitoring:compelte];
}

- (void)sendDataByLauch{
    NSString *limit=[NSString stringWithFormat:@"%ld",(long)[[OdinConfigManager shareInstance] getSendCount:OdinAppStatusStart]];
    [[OdinSqlLiteManager shareInstance] selectKeys:@[@{@"id":Odin_SQL_DATATYPE_INTRGER},
  @{@"headInfo":Odin_SQL_DATATYPE_TEXT},
  @{@"bodyInfo":Odin_SQL_DATATYPE_TEXT},
  @{@"type":Odin_SQL_DATATYPE_TEXT},
  @{@"duStartTime":Odin_SQL_DATATYPE_TEXT}] fromTable:@"AodEvenDataTb" orderBy:@"stat_time" orderType:Odin_SQL_ORDERTYPE_ASC whereString:@"sdkInitComplete=1" limit:limit complete:^(NSArray * _Nonnull dataArr, NSError * _Nonnull error) {
        if (dataArr.count>0) {
            NSString *sendString=  [[OdinDataManager shareInstance] sendDataString:dataArr];
            NSString *url=[NSString stringWithFormat:@"%@%@",OdinCommonLinkUrl,OdinSendData];
            [OdinHttp post:url bodyString:sendString success:^(id  _Nullable responseObject) {
                //提交成功后删除本地数据
                NSArray *arr=[dataArr valueForKeyPath:@"id"];
                NSString *whereStr=[NSString stringWithFormat:@"id in (%@)",[arr componentsJoinedByString:@","] ];
                [[OdinSqlLiteManager shareInstance]  deleteDataFromTable:@"AodEvenDataTb" whereString:whereStr];
                [[OdinLogHelper sharedInstance] writeDataWithType:localLogType_datasend text:[NSString aod_jsonString:dataArr]];
            } failure:^(NSError * _Nonnull error) {
                if (error) {
                    Odin_Error(@"提交到服务器失败");
                }
            }];
        }
    }];
}

#pragma mark --private method
- (void)loadServerConfig:(void (^)(void))compelte{
    //服务器获取
    NSString *url=[NSString stringWithFormat:@"%@%@",OdinCommonLinkUrl,OdinGetConfig];
    [OdinHttp post:url parameters:@{@"appKey":[OdinDataManager shareInstance].headDic[@"appkey"]} success:^(id  _Nullable responseObject) {
        NSMutableDictionary *configDic=[responseObject[@"sendConfig"] mutableCopy];
        [configDic setObject:responseObject[@"backgroundLiveTime"] forKey:@"backgroundLiveTime"];
        [configDic setObject:responseObject[@"dataExpiretime"] forKey:@"dataExpiretime"];
        OdinConfig *config=[OdinConfig aod_modelWithDict:configDic];
        [OdinConfigManager shareInstance].useConfig=config;
        //删除之前的然后 存本地
        BOOL success= [[OdinSqlLiteManager shareInstance] deleteDataFromTable:@"OdinConfigTb" whereString:nil];
        if (success) {
            [configDic setObject:[OdinTimeTool getCheckCurrentTime] forKey:@"updateTime"];
            [[OdinSqlLiteManager shareInstance] insertData:configDic intoTable:@"OdinConfigTb"];
        }else{
        }
        Odin_Debug(@"config :%@",responseObject);
        if (compelte) {
            compelte();
        }
    } failure:^(NSError * _Nonnull error) {
        if (compelte) {
            compelte();
        }
        Odin_Error(@"获取服务配置失败");
        if (error) {
            //查找本地 本地有使用本地 没有使用默认
                [[OdinSqlLiteManager shareInstance] selectKeys:nil fromTable:@"OdinConfigTb" orderBy:nil orderType:nil whereString:nil limit:nil complete:^(NSArray * _Nonnull dataArr, NSError * _Nonnull error) {
                    if (dataArr.count>0) {//本地有
                        OdinConfig *config=[OdinConfig aod_modelWithDict:dataArr.firstObject];
                        //使用本地
                        [OdinConfigManager shareInstance].useConfig=config;
                    }else{
                        //使用默认策略
                        [OdinConfigManager shareInstance].useConfig=[OdinConfigManager shareInstance].defaultConfig;
                    }
                }];
           
        }
    }];
}

- (void)startTimer:(OdinAppStatus)appStatus{
//     [self.timer setFireDate:[NSDate distantPast]];
    
    [self stopTimer];
    NSInteger timerInterval=[[OdinConfigManager shareInstance] getTimeInterval:self.appStatus]/1000;
    Odin_Info(@"定时器时间：%ld",(long)timerInterval);
    if (timerInterval<10) {
        timerInterval=30;
    }
//    if (appStatus==OdinAppStatusStart) {
////        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(timerInterval * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
////            self.timer=[NSTimer timerWithTimeInterval:timerInterval target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
////            [[NSRunLoop currentRunLoop] addTimer:self->_timer forMode:NSRunLoopCommonModes];
////        });
//        self.timer=[NSTimer timerWithTimeInterval:timerInterval target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
//        [[NSRunLoop currentRunLoop] addTimer:self->_timer forMode:NSRunLoopCommonModes];
//    }else{
//        self.timer=[NSTimer timerWithTimeInterval:timerInterval target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
//        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
//    }
   
    self.timer=[NSTimer timerWithTimeInterval:timerInterval target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void)stopTimer{
//    [self.timer setFireDate:[NSDate distantFuture]];
    [self.timer invalidate];
    self.timer=nil;
}

- (void)timerAction:(NSTimer *)sender{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
       
        NSString *limit=[NSString stringWithFormat:@"%ld",(long)[[OdinConfigManager shareInstance] getSendCount:OdinAppStatusUsing]];
        [[OdinSqlLiteManager shareInstance] selectKeys:@[@{@"id":Odin_SQL_DATATYPE_INTRGER},
  @{@"headInfo":Odin_SQL_DATATYPE_TEXT},
  @{@"bodyInfo":Odin_SQL_DATATYPE_TEXT},
  @{@"type":Odin_SQL_DATATYPE_TEXT},
  @{@"duStartTime":Odin_SQL_DATATYPE_TEXT}] fromTable:@"AodEvenDataTb"
                                              orderBy:@"stat_time"
                                            orderType:Odin_SQL_ORDERTYPE_ASC
                                          whereString:@"sdkInitComplete=1"
                                                limit:limit
                                             complete:^(NSArray * _Nonnull dataArr, NSError * _Nonnull error) {
            if (dataArr.count>0) {
                NSString *sendString=  [[OdinDataManager shareInstance] sendDataString:dataArr];
                NSString *url=[NSString stringWithFormat:@"%@%@",OdinCommonLinkUrl,OdinSendData];
                [OdinHttp post:url bodyString:sendString success:^(id  _Nullable responseObject) {
                    
                    NSArray *arr=[dataArr valueForKeyPath:@"id"];
                    NSString *whereStr=[NSString stringWithFormat:@"id in (%@)",[arr componentsJoinedByString:@","] ];
                    [[OdinSqlLiteManager shareInstance]  deleteDataFromTable:@"AodEvenDataTb" whereString:whereStr];
                    
                    [[OdinLogHelper sharedInstance] writeDataWithType:localLogType_datasend text:[NSString aod_jsonString:dataArr]];
                } failure:^(NSError * _Nonnull error) {
                    if (error) {
                        Odin_Error(@"提交到服务器失败");
                    }
                }];
            }
        }];
        
    });
}

@end
