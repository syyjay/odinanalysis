//
//  AodDefineHeader.h
//  OdinAnalysisSDK
//
//  Created by nathan on 2019/2/28.
//  Copyright © 2019 Odin. All rights reserved.
//
//
//
//111.172.62.253:9211  ，内网地址  192.168.121.49:9211  ，采用轮询方式负载到5台机器上

#ifndef OdinDefineHeader_h
#define OdinDefineHeader_h
#define OdinSDkVersion @"1.0"
#define OdinSDKTYPEMDE @"SupportTest"
//网络请求相关 内网：192.168.124.236:8060  外网:111.172.62.253:9211
#define OdinCommonLinkUrl           @"http://47.107.93.116/api/sdk-service/" //192.168.121.49:9211
#define OdinSendData                @"/ibd/sdk/data-uploading"
#define OdinGetNetDate              @"/ibd/sdk/get-date"
#define OdinGetConfig               @"/ibd/sdk/get-allocation-strategy"
#define OdinGetRegisterChannel      @"/ibd/sdk/query-channel"

#endif /* OdinDefineHeader_h */
