//
//  AodSqlLiteManager.h
//  AodanalysisSDK
//
//  Created by nathan on 2019/2/26.
//  Copyright © 2019 Odin. All rights reserved.
//

#import <Foundation/Foundation.h>

#define     OdinSqlieName    @"OdinSDKData.sqlite"
#define     OdinMaxCount     5000


#define Odin_SQL_DATATYPE_SMALLINT @"smallint" //short
#define Odin_SQL_DATATYPE_INTRGER @"integer"    //int
#define Odin_SQL_DATATYPE_REAL @"real"          //实数
#define Odin_SQL_DATATYPE_FLOAT @"float"        //float
#define Odin_SQL_DATATYPE_DOUBLE @"double"      //double
#define Odin_SQL_DATATYPE_CURRENCY @"currency"  //long
#define Odin_SQL_DATATYPE_VARCHAR @"varchar"    //char
#define Odin_SQL_DATATYPE_TEXT @"text"          //string
#define Odin_SQL_DATATYPE_BINARY @"binary"      //二进制
#define Odin_SQL_DATATYPE_BLOB @"blob"          //长二进制
#define Odin_SQL_DATATYPE_BOOLEAN @"boolean"    //bool
#define Odin_SQL_DATATYPE_DATE @"date"          //日期
#define Odin_SQL_DATATYPE_TIME @"time"          //时间
#define Odin_SQL_DATATYPE_TIMESTAMP @"timestamp"//时间戳

#define Odin_SQL_ORDERTYPE_ASC @"asc" //升序
#define Odin_SQL_ORDERTYPE_DESC @"desc" //降序

NS_ASSUME_NONNULL_BEGIN

@interface OdinSqlLiteManager : NSObject

+(instancetype)shareInstance;
/**
 打开数据库

 @return 是否打开成功
 */
- (BOOL)openDB;

/**
 给表添加一条数据
 
 @param dataDic  要添加的数据的对应的字段和值
 @param tableName 表名
 @return 是否执行成功
 */
- (BOOL)insertData:(NSDictionary *)dataDic intoTable:(NSString *)tableName;

/**
 更新数据

 @param dataDic 要更新的数据的对应的字段和值
 @param tableName 表名
 @param whereStr 更新条件
 @return 是否执行成功
 */
-(BOOL)updateData:(NSDictionary *)dataDic inTable:(NSString *)tableName whereString:(NSString *_Nullable)whereStr;

/**
 查询数据

 @param keys 要查询的字段
 @param tableName 查询的表名
 @param orderKey 排序字段
 @param type 排序类型
 @param whereString 查询条件
 */
-(void)selectKeys:(NSArray<NSDictionary *> *_Nullable)keys fromTable:(NSString *_Nullable)tableName orderBy:(NSString *_Nullable)orderKey orderType:(NSString *_Nullable)type whereString:(NSString *_Nullable)whereString limit:(NSString *_Nullable)limitString complete:(void (^)(NSArray *dataArr,NSError *error))complete;


/**
 删除数据

 @param tableName 表名
 @param whereString 删除的条件
 @return 是否删除成功
 */
-(BOOL)deleteDataFromTable:(NSString *)tableName whereString:(NSString *_Nullable)whereString;
@end

NS_ASSUME_NONNULL_END
