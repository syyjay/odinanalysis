//
//  AodSqlLiteManager.m
//  AodanalysisSDK
//
//  Created by nathan on 2019/2/26.
//  Copyright © 2019 Odin. All rights reserved.
//

#import "OdinSqlLiteManager.h"
#import <sqlite3.h>

@implementation OdinSqlLiteManager{
    sqlite3 *_database;
}

static OdinSqlLiteManager *instance;
+ (id)allocWithZone:(struct _NSZone *)zone
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [super allocWithZone:zone];
    });
    
    return instance;
}

+ (instancetype)shareInstance
{
    if (instance == nil) {
        instance = [[OdinSqlLiteManager alloc] init];
    }
    
    return instance;
}

- (BOOL)openDB{
    //app内数据库文件存放路径-一般存放在沙盒中
    NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString *DBPath = [documentPath stringByAppendingPathComponent:OdinSqlieName];
    //创建(指定路径不存在数据库文件,不存在sqlite3_open会自动新建)/打开(已存在数据库文件) 数据库文件
    sqlite3_shutdown();
    int err= sqlite3_config(SQLITE_CONFIG_SERIALIZED);
    sqlite3_initialize();
    if (err == SQLITE_OK) {
        Odin_Info(@"Can now use sqlite on multiple threads, using the same connection");
    } else {
        Odin_Info(@"setting sqlite thread safe mode to serialized failed!!! return code: %d", err);
    }
    if (sqlite3_open_v2(DBPath.UTF8String, &_database, SQLITE_OPEN_READWRITE|SQLITE_OPEN_FULLMUTEX|SQLITE_OPEN_CREATE|SQLITE_OPEN_WAL, NULL) != SQLITE_OK) {
        //数据库打开失败,关闭数据库
        sqlite3_close(_database);
        Odin_Debug(@"数据库打开失败");
        return NO;
    }else{
        Odin_Debug(@"数据库打开成功");
        //打开成功创建表
        return [self createTable];
    }
}

- (BOOL)createTable{
    NSString *configTableSql=@"create table if not exists OdinConfigTb (id INTEGER PRIMARY KEY AUTOINCREMENT,backgroundLiveTime INTEGER, dataExpiretime INTEGER, wifiBackgroundCount INTEGER, wifiBackgroundIntervalTime INTEGER, wifiIntervalCount INTEGER ,wifiIntervalTime INTEGER ,wifiStartCount INTEGER , wwanBackgroundCount INTEGER, wwanBackgroundIntervalTime INTEGER, wwanIntervalCount INTEGER ,wwanIntervalTime INTEGER ,wwanStartCount INTEGER,updateTime TEXT)";
    
       NSString *evenDataTableSql=@"create table if not exists AodEvenDataTb (id INTEGER PRIMARY KEY AUTOINCREMENT,headInfo TEXT, bodyInfo TEXT, type TEXT,session_id TEXT, sessionType INTEGER, stat_time TEXT ,duStartTime TEXT,status INTEGER ,du BIGINT default 0, prepage_id TEXT, page_id TEXT, eventId TEXT ,order_number TEXT ,amount INTEGER,title TEXT,context TEXT,category TEXT,user_id TEXT,sdkInitComplete INTEGER default 1)";
    BOOL success=[self runSQL:configTableSql]&&[self runSQL:evenDataTableSql];
    Odin_Debug(@"%@",success?@"表创建成功":@"表创建失败");
    return success;
   
}

- (BOOL)insertData:(NSDictionary *)dataDic intoTable:(NSString *)tableName{
    NSMutableString *keys = [[NSMutableString alloc]init];
    NSMutableString *values = [[NSMutableString alloc]init];
    for (NSInteger i=0; i<dataDic.allKeys.count; i++) {
        NSString * key = dataDic.allKeys[i];
        if (i<dataDic.count-1) {
            [keys appendFormat:@"%@,",key];
            [values appendFormat:@"'%@',",[dataDic objectForKey:key]];
        }else{
            [keys appendFormat:@"%@",key];
            [values appendFormat:@"'%@'",[dataDic objectForKey:key]];
        }
    }
    NSString * sqlStr = [NSString stringWithFormat:@"insert into %@(%@) values(%@)",tableName,keys,values];
    BOOL success=[self runSQL:sqlStr];
    Odin_Debug(@"insert Data:%@,%@",sqlStr,success?@"成功":@"失败");
    return success;
}

-(BOOL)updateData:(NSDictionary *)dataDic inTable:(NSString *)tableName whereString:(NSString *_Nullable)whereStr{
    NSMutableString * sqlStr = [[NSMutableString alloc]init];
    [sqlStr appendFormat:@"update %@ set ",tableName];
    for (int i=0; i<dataDic.allKeys.count; i++) {
        NSString * key = dataDic.allKeys[i];
        if (i<dataDic.allKeys.count-1) {
            [sqlStr appendFormat:@"%@='%@',",key,[dataDic objectForKey:key]];
        }else{
            [sqlStr appendFormat:@"%@='%@'",key,[dataDic objectForKey:key]];
            if (whereStr!=nil) {
                [sqlStr appendFormat:@" where %@",whereStr];
            }
        }
    }
    BOOL success=[self runSQL:sqlStr];
    Odin_Debug(@"update Data:%@,%@",sqlStr,success?@"成功":@"失败");
    return success;
}

-(void)selectKeys:(NSArray<NSDictionary *> *_Nullable)keys fromTable:(NSString * _Nullable )tableName orderBy:(NSString *_Nullable)orderKey orderType:(NSString *_Nullable)type whereString:(NSString *_Nullable)whereString  limit:(NSString *_Nullable)limitString complete:(void (^)(NSArray *dataArr,NSError *error))complete{
    NSMutableString * sqlStr = [[NSMutableString alloc]init];
    [sqlStr appendFormat:@"select"];
    if (keys==nil||keys.count==0) {
        [sqlStr appendFormat:@" * from %@",tableName];
    }else{
        for (int i=0; i<keys.count; i++) {
            if (i<keys.count-1) {
                [sqlStr appendFormat:@" %@,",keys[i].allKeys.firstObject];
            }else{
                [sqlStr appendFormat:@" %@ from %@",keys[i].allKeys.firstObject,tableName];
            }
            
        }
    }
    if (whereString) {
        [sqlStr appendFormat:@" where %@",whereString];
    }
    if (orderKey) {
        [sqlStr appendFormat:@" order by %@",orderKey];
    }
    if (type) {
        [sqlStr appendFormat:@" %@",type];
    }
    if (limitString) {
        [sqlStr appendFormat:@" LIMIT %@",limitString];
    }
    NSMutableArray * keysArr = [[NSMutableArray alloc]init];
    NSMutableArray * keysTypeArr = [[NSMutableArray alloc]init];
    if (keys==nil||keys.count==0) {
        NSArray<NSDictionary *> * tmpArr = [self getTheTableAllKeys:tableName];
        for (int i=0; i<tmpArr.count; i++) {
            NSString * key = tmpArr[i].allKeys.firstObject;
            [keysArr addObject:key];
            [keysTypeArr addObject:[tmpArr[i] objectForKey:key]];
        }
    }else{
        for (int i=0; i<keys.count; i++) {
            NSString * key = keys[i].allKeys.firstObject;
            [keysArr addObject:key];
            [keysTypeArr addObject:[keys[i] objectForKey:key]];
        }
    }
    
    [self runSelectSQL:sqlStr withKeys:keysArr withDataType:keysTypeArr complete:^(NSArray *dataArr, NSError *error) {
        Odin_Debug(@"%@ \r\n %@",sqlStr,dataArr);
        if (complete) {
            complete(dataArr,error);
        }
    }];
}

-(BOOL)deleteDataFromTable:(NSString *)tableName whereString:(NSString *_Nullable)whereString {
    NSMutableString * sqlStr = [[NSMutableString alloc]init];
    [sqlStr appendFormat:@"delete from %@",tableName];
    if (whereString!=nil) {
        [sqlStr appendFormat:@" where %@",whereString];
    }
    BOOL success=[self runSQL:sqlStr];
    Odin_Debug(@"delete Data:%@,%@",sqlStr,success?@"成功":@"失败");
    return  success;
}

#pragma mark --privateMethod
/**
 执行非查询结果集的的sql

 @param sql 执行语句
 @return 是否执行成功
 */
-(BOOL)runSQL:(NSString *)sql{
    char * err;
    int code = sqlite3_exec(_database, [sql UTF8String], NULL, NULL, &err);
    if (code!=SQLITE_OK) {
        return NO;
    }else{
        return YES;
    }
}

-(void)runSelectSQL:(NSString *)sql withKeys:(NSArray *_Nullable)keys withDataType:(NSArray *_Nullable)dataType complete:(void (^)(NSArray *dataArr,NSError *error))complete{
    sqlite3_stmt *stmt =nil;
    int code = sqlite3_prepare_v2(_database, [sql UTF8String], -1, &stmt, NULL);
    if (code!=SQLITE_OK) {
        
    }else{
        NSMutableArray * resultArray = [[NSMutableArray alloc]init];
        
        while (sqlite3_step(stmt)==SQLITE_ROW) {
            //数据类型的分别解析
            NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
            for (int i=0; i<dataType.count; i++) {
                NSString * type = dataType[i];
                type=[type lowercaseString];
                if ([type isEqualToString:Odin_SQL_DATATYPE_BINARY]) {
                    int length = sqlite3_column_bytes(stmt, i);
                    const void *data = sqlite3_column_blob(stmt, i);
                    NSData * value = [NSData dataWithBytes:data length:length];
                    [dic  setObject:value forKey:keys[i]];
                }else if([type isEqualToString:Odin_SQL_DATATYPE_BLOB]){
                    int length = sqlite3_column_bytes(stmt, i);
                    const void *data = sqlite3_column_blob(stmt, i);
                    NSData * value = [NSData dataWithBytes:data length:length];
                    [dic  setObject:value forKey:keys[i]];
                }else if([type isEqualToString:Odin_SQL_DATATYPE_BOOLEAN]){
                    NSNumber * value = [NSNumber numberWithInt:sqlite3_column_int(stmt, i)];
                    [dic setObject:value forKey:keys[i]];
                }else if([type isEqualToString:Odin_SQL_DATATYPE_CURRENCY]){
                    NSNumber * value = [NSNumber numberWithLongLong:sqlite3_column_int64(stmt, i)];
                    [dic setObject:value forKey:keys[i]];
                }else if([type isEqualToString:Odin_SQL_DATATYPE_DATE]){
                    char * cString =(char*)sqlite3_column_text(stmt, i);
                    NSString * value = [NSString stringWithCString:cString?cString:"NULL" encoding:NSUTF8StringEncoding];
                    [dic setObject:value forKey:keys[i]];
                }else if([type isEqualToString:Odin_SQL_DATATYPE_DOUBLE]){
                    NSNumber * value = [NSNumber numberWithFloat:sqlite3_column_double(stmt, i)];
                    [dic setObject:value forKey:keys[i]];
                }else if([type isEqualToString:Odin_SQL_DATATYPE_FLOAT]){
                    NSNumber * value = [NSNumber numberWithFloat:sqlite3_column_double(stmt, i)];
                    [dic setObject:value forKey:keys[i]];
                }else if([type isEqualToString:Odin_SQL_DATATYPE_INTRGER]){

                    NSNumber * value = [NSNumber numberWithInt:sqlite3_column_int(stmt, i)];
                    [dic setObject:value forKey:keys[i]];
                }else if([type isEqualToString:Odin_SQL_DATATYPE_REAL]){
                    NSNumber * value = [NSNumber numberWithDouble:sqlite3_column_int(stmt, i)];
                    [dic setObject:value forKey:keys[i]];
                }else if([type isEqualToString:Odin_SQL_DATATYPE_SMALLINT]){
                    NSNumber * value = [NSNumber numberWithShort:sqlite3_column_int(stmt, i)];
                    [dic setObject:value forKey:keys[i]];
                }else if([type isEqualToString:Odin_SQL_DATATYPE_TEXT]){
                    char * cString =(char*)sqlite3_column_text(stmt, i);
                    NSString * value = [NSString stringWithCString:cString?cString:"NULL" encoding:NSUTF8StringEncoding];
                    [dic setObject:value forKey:keys[i]];
                }else if([type isEqualToString:Odin_SQL_DATATYPE_TIME]){
                    char * cString =(char*)sqlite3_column_text(stmt, i);
                    NSString * value = [NSString stringWithCString:cString?cString:"NULL" encoding:NSUTF8StringEncoding];
                    [dic setObject:value forKey:keys[i]];
                }else if([type isEqualToString:Odin_SQL_DATATYPE_TIMESTAMP]){
                    NSNumber * value = [NSNumber numberWithLongLong:sqlite3_column_int64(stmt, i)];
                    [dic setObject:value forKey:keys[i]];
                }else if([type isEqualToString:Odin_SQL_DATATYPE_VARCHAR]){
                    char * cString =(char*)sqlite3_column_text(stmt, i);
                    NSString * value = [NSString stringWithCString:cString?cString:"NULL" encoding:NSUTF8StringEncoding];
                    [dic setObject:value forKey:keys[i]];
                }

            }
            [resultArray addObject:dic];
        }
        sqlite3_finalize(stmt);
        stmt=nil;
        complete(resultArray,nil);
    }
}

//获取表中所有字段名和类型
-(NSArray<NSDictionary *> *)getTheTableAllKeys:(NSString *)tableName{
    NSMutableArray * array = [[NSMutableArray alloc]init];
    NSString * getColumn = [NSString stringWithFormat:@"PRAGMA table_info(%@)",tableName];
    sqlite3_stmt *statement;
    sqlite3_prepare_v2(_database, [getColumn UTF8String], -1, &statement, nil);
    while (sqlite3_step(statement) == SQLITE_ROW) {
        char *nameData = (char *)sqlite3_column_text(statement, 1);
        NSString *columnName = [[NSString alloc] initWithUTF8String:nameData];
        char *typeData = (char *)sqlite3_column_text(statement, 2);
        NSString *columntype = [NSString stringWithCString:typeData encoding:NSUTF8StringEncoding];
        NSDictionary * dic = @{columnName:columntype};
        [array addObject:dic];
    }
    sqlite3_finalize(statement);
    statement=nil;
    return array;
}
@end
