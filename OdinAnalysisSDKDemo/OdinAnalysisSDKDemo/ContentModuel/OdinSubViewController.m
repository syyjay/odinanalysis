//
//  OdinSubViewController.m
//  OdinAnalysisSDKDemo
//
//  Created by nathan on 2019/5/15.
//  Copyright © 2019 Odin. All rights reserved.
//

#import "OdinSubViewController.h"

@interface OdinSubViewController ()
@property(nonatomic,strong)UILabel *label;
@end

@implementation OdinSubViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.label=[[UILabel alloc]init];
    [self.view addSubview:self.label];
    
    self.label.text=self.content;
    self.label.center=self.view.center;
    [self.label sizeToFit];
    
    int R = (arc4random() % 256) ;
    int G = (arc4random() % 256) ;
    int B = (arc4random() % 256) ;

    
    self.view.backgroundColor=[UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:1];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [OdinAnalysisSDK pageStart:self.content];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [OdinAnalysisSDK pageEnd:self.content];
}

@end
