//
//  OdinContentViewController.m
//  OdinAnalysisSDKDemo
//
//  Created by nathan on 2019/5/15.
//  Copyright © 2019 Odin. All rights reserved.
//

#import "OdinContentViewController.h"
#import "OdinSubViewController.h"
@interface OdinContentViewController ()<UIPageViewControllerDelegate, UIPageViewControllerDataSource>{
    OdinSubViewController *aVc;
    OdinSubViewController *bVc;
    OdinSubViewController *cVc;
    NSArray *vcs;
    NSInteger currentIndex;
    UIViewController *pendvc;
}
@property(nonatomic,strong)  UIPageViewController *pageVc;
@end

@implementation OdinContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
       NSDictionary * options = [NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:UIPageViewControllerSpineLocationNone] forKey:UIPageViewControllerOptionSpineLocationKey];
    UIPageViewController *pageVc = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll    navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:options];  //pageCurl卷页效果
   
    self.pageVc=pageVc;
    aVc = [OdinSubViewController new];
    aVc.content=@"AVC";
    aVc.view.frame=self.view.bounds;
    
    bVc = [OdinSubViewController new];
    bVc.content=@"BVC";
    bVc.view.frame=self.view.bounds;
    
    cVc = [OdinSubViewController new];
    cVc.content=@"CVC";
    cVc.view.frame=self.view.bounds;
    
    pageVc.delegate = self;
    pageVc.dataSource = self;
    vcs = @[aVc,bVc,cVc];
    [pageVc setViewControllers:@[aVc] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    pageVc.view.frame = self.view.bounds;
    [self addChildViewController:pageVc];
    [self.view addSubview:pageVc.view];
     [pageVc didMoveToParentViewController:self];
}

#pragma mark UIPageViewControllerDataSource
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    NSInteger after = currentIndex + 1;
    if (after >= vcs.count) {
        return nil;
    }
    return vcs[after];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    NSInteger before = currentIndex - 1;
    if (before < 0) {
        return nil;
    }
    return vcs[before];
}

#pragma mark UIPageViewControllerDelegate
- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray<UIViewController *> *)pendingViewControllers{
    pendvc = pendingViewControllers.firstObject;
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed{
    [vcs enumerateObjectsUsingBlock:^(UIViewController *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj == pendvc) {  //判断视图控制器是否与正在转换的视图控制器为同一个
            currentIndex = idx;
            *stop = YES;
        }
    }];
}
/***以下两个delegate方式在卷页过渡效果时设置UIPageViewControllerTransitionStylePageCurl***/
//返回视图控制器支持的所有方向
- (UIInterfaceOrientationMask)pageViewControllerSupportedInterfaceOrientations:(UIPageViewController *)pageViewController {
    return UIInterfaceOrientationMaskLandscape;
}

//返回视图控制器的首选方向
- (UIInterfaceOrientation)pageViewControllerPreferredInterfaceOrientationForPresentation:(UIPageViewController *)pageViewController {
    return UIInterfaceOrientationLandscapeRight;
}


@end
