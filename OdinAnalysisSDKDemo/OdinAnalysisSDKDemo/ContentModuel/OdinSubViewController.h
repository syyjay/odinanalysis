//
//  OdinSubViewController.h
//  OdinAnalysisSDKDemo
//
//  Created by nathan on 2019/5/15.
//  Copyright © 2019 Odin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OdinSubViewController : UIViewController
@property(nonatomic,copy)NSString *content;
@end

NS_ASSUME_NONNULL_END
