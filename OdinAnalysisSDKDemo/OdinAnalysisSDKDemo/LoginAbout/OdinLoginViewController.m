//
//  OdinLoginViewController.m
//  OdinAnalysisSDKDemo
//
//  Created by nathan on 2019/5/15.
//  Copyright © 2019 Odin. All rights reserved.
//

#import "OdinLoginViewController.h"

@interface OdinLoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameT;
@property (weak, nonatomic) IBOutlet UITextField *pwdT;
@property (weak, nonatomic) IBOutlet UIButton *actionBtn;

@end

@implementation OdinLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if (self.regist) {
        [self.actionBtn setTitle:@"注册" forState:0];
    }
}



- (IBAction)LoginAction:(UIButton *)sender {
    if (self.regist) {
        [OdinAnalysisSDK eventRegister:self.nameT.text];
        [self showText:@"注册"];
    }else{
        
        [OdinAnalysisSDK eventLogin:self.nameT.text status:1];
        [self showText:@"登录"];
    }
}

- (void)showText:(NSString *)text{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    // Set the text mode to show only text.
    hud.mode = MBProgressHUDModeText;
    hud.label.text = text;
    // Move to bottm center.
    hud.offset = CGPointMake(0.f, MBProgressMaxOffset);
    
    [hud hideAnimated:YES afterDelay:1.5f];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
