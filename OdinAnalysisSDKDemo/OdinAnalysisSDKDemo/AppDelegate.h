//
//  AppDelegate.h
//  OdinAnalysisSDKDemo
//
//  Created by nathan on 2019/5/15.
//  Copyright © 2019 Odin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

