//
//  TableViewCell.m
//  OdinAnalysisSDKDemo
//
//  Created by isec on 2019/5/15.
//  Copyright © 2019 Odin. All rights reserved.
//

#import "TableViewCell.h"

@implementation TableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
