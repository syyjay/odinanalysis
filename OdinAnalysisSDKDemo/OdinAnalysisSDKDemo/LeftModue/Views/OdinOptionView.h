//
//  OdinOptionView.h
//  OdinAnalysisSDKDemo
//
//  Created by isec on 2019/5/15.
//  Copyright © 2019 Odin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OdinOptionView : UIView

- (void)showInView:(UIView *)view;

@property(nonatomic,copy) void (^didSelectBlock)(NSIndexPath *indexPath);
@end

NS_ASSUME_NONNULL_END
