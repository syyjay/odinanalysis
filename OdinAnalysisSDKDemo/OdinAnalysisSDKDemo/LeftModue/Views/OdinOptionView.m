//
//  OdinOptionView.m
//  OdinAnalysisSDKDemo
//
//  Created by isec on 2019/5/15.
//  Copyright © 2019 Odin. All rights reserved.
//

#import "OdinOptionView.h"
#import "TableViewCell.h"
#import "OdinHeaderView.h"

@interface OdinOptionView ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSArray*dataSource;

@end



@implementation OdinOptionView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 300, CGRectGetHeight(frame)) style:UITableViewStylePlain];
        [self.tableView registerNib:[UINib nibWithNibName:@"TableViewCell" bundle:nil] forCellReuseIdentifier:@"TableViewCell"];
//        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        [self addSubview:self.tableView];
        
        OdinHeaderView *header = [NSBundle.mainBundle loadNibNamed:@"OdinHeaderView" owner:nil options:nil].firstObject;
        header.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 80);
        self.tableView.tableHeaderView = header;
        
        NSString *path = [NSBundle.mainBundle pathForResource:@"analyse.json" ofType:nil];
        _dataSource = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:path] options:NSJSONReadingAllowFragments error:nil];
    }
    return self;
}

-(void)showInView:(UIView *)view {
    if (!view) {
        return;
    }
    [view addSubview:self];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self removeFromSuperview];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identity = @"TableViewCell";
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identity forIndexPath:indexPath];
    NSDictionary *info = [[_dataSource[indexPath.section] objectForKey:@"datas"] objectAtIndex:indexPath.row];
    cell.contentLabel.text = info[@"title"];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[_dataSource[section] objectForKey:@"datas"] count];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *title = [[_dataSource objectAtIndex:section] objectForKey:@"headerTitle"];
    if (title.length > 0) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 55)];
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, 0.5)];
        lineView.backgroundColor = UIColor.lightGrayColor;
        [view addSubview:lineView];
        UILabel *titleLabel = UILabel.new;
        titleLabel.text = title;
        titleLabel.textColor = UIColor.darkGrayColor;
        [titleLabel sizeToFit];
        view.backgroundColor=[UIColor groupTableViewBackgroundColor];
        titleLabel.frame = CGRectMake(20, (view.frame.size.height - titleLabel.frame.size.height) * 0.5, titleLabel.frame.size.width, titleLabel.frame.size.height);
        [view addSubview:titleLabel];
        return view;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    NSString *title = [[_dataSource objectAtIndex:section] objectForKey:@"headerTitle"];
    if (title.length > 0) {
        return 55;
    }
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.didSelectBlock) {
        self.didSelectBlock(indexPath);
    }
}
@end
