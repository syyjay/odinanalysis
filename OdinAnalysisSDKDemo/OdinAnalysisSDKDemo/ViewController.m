//
//  ViewController.m
//  OdinAnalysisSDKDemo
//
//  Created by nathan on 2019/5/15.
//  Copyright © 2019 Odin. All rights reserved.
//

#import "ViewController.h"
#import "OdinOptionView.h"
#import "LoginAbout/OdinLoginViewController.h"
#import "ContentModuel/OdinContentViewController.h"
@interface ViewController ()
@property (nonatomic, strong) OdinOptionView *optView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
}

- (IBAction)action:(UIButton *)sender {
    _optView = [[OdinOptionView alloc] initWithFrame:self.view.bounds];
    [_optView showInView:self.view];
    __weak typeof(self) weakSelf=self;
    _optView.didSelectBlock = ^(NSIndexPath *indexPath) {
        NSLog(@"%ld %ld",(long)indexPath.section,(long)indexPath.row);
        if (indexPath.section==0) {
            switch (indexPath.row) {
                case 0:
                    //操作事件
                {
                    [OdinAnalysisSDK eventOperation:@"单击事件"];
                    [weakSelf showText:@"单击事件"];
                    
                }
                    break;
                case 1:
                    //界面切换
                    [weakSelf.navigationController pushViewController:[OdinContentViewController new] animated:YES];
                    break;
                case 2:
                    //自定义事件
                    [OdinAnalysisSDK eventCustom:@"自定义事件" attributes:@{@"自定义key":@"自定义value"}];
                    [weakSelf showText:@"自定义事件"];
                    break;
                case 3:
                    //支付
                    [OdinAnalysisSDK eventPay:12 orderNumber:@"NO.2000001" amount:@"100"];
                    [weakSelf showText:@"支付事件"];
                    break;
                default:
                    break;
            }
        }
        
        if (indexPath.section==1) {
            switch (indexPath.row) {
                case 0:
                    //登录
                {
                    OdinLoginViewController *vc=[OdinLoginViewController new];
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                }
                    break;
                case 1:
                    //注册
                {
                    OdinLoginViewController *vc=[OdinLoginViewController new];
                    vc.regist=YES;
                    [weakSelf.navigationController pushViewController:vc animated:YES];
                    
                }
                    break;
                default:
                    break;
            }
        }
        
        if (indexPath.section==2) {
            switch (indexPath.row) {
                case 0:
                    //用户反馈错误
                    [OdinAnalysisSDK eventErrorTitle:@"用户反馈" context:@"用户返回错误"];
                    break;
                case 1:
                    //软件异常
                {
                    NSMutableArray *arr=[NSMutableArray array];
                    [arr addObject:nil];
                }
                break;
                default:
                    break;
            }
        }
    };
}

- (void)showText:(NSString *)text{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    // Set the text mode to show only text.
    hud.mode = MBProgressHUDModeText;
    hud.label.text = text;
    // Move to bottm center.
    hud.offset = CGPointMake(0.f, MBProgressMaxOffset);
    
    [hud hideAnimated:YES afterDelay:1.5f];
}

@end
